dermvedaApp.factory('documentService', ['$http', 'config', '$q', function($http, config, $q) {

    var documents = function(tag, max) {
        var articleSection = {};
        var subArticles = [];
        var promisses = [];
        if (max !== -1) {
            request = config.apiUrl + '?_query=' + tag + '&_max=' + max;
        } else {
            request = config.apiUrl + '?_query=' + tag;
        }
        $http.get(request).then(function(response) {
            angular.forEach(response.data.items, function(val, key) {
                promisses.push($http({
                    method: 'GET',
                    url: val.link.url
                }));
            });

            $q.all(promisses).then(function(result) {
                angular.forEach(result, function(val, key) {
                    if (!articleSection.main) {
                        articleSection.main = {};
                        articleSection.main.title = val.data.items['dermveda:title'].length < 38 ? val.data.items['dermveda:title'] : val.data.items['dermveda:title'].substring(0, 38) + '..';
                        articleSection.main.introduction = val.data.items['dermveda:introduction'];
                        articleSection.main.mainImageUrl = val.data.items['dermveda:mainimage'].link.url;
                        articleSection.main.id = val.data['id'];
                        articleSection.main.taxonomy = val.data.items['hippotaxonomy:canonkey'];
                        articleSection.main.tags = val.data.items['hippostd:tags'][0];
                    } else {
                        var subArticle = {};
                        subArticle.title = val.data.items['dermveda:title'];
                        subArticle.introduction = val.data.items['dermveda:introduction'];
                        subArticle.mainImageUrl = val.data.items['dermveda:mainimage'].link.url;
                        subArticle.id = val.data['id'];
                        subArticle.taxonomy = val.data.items['hippotaxonomy:canonkey'];
                        subArticle.tags = val.data.items['hippostd:tags'][0];
                        subArticles.push(subArticle);
                    }
                })
            });
        });

        articleSection.sub = subArticles;
        return articleSection;
    }

    var herbalCorner = function(tag, max) {
        var articleSection = {};
        var subArticles = [];
        var promisses = [];
        if (max !== -1) {
            request = config.apiUrl + '?_query=' + tag + '&_max=' + max;
        } else {
            request = config.apiUrl + '?_query=' + tag;
        }
        $http.get(request).then(function(response) {
            angular.forEach(response.data.items, function(val, key) {
                promisses.push($http({
                    method: 'GET',
                    url: val.link.url
                }));
            });

            $q.all(promisses).then(function(result) {
                angular.forEach(result, function(val, key) {
                    var subArticle = {};
                    subArticle.title = val.data.items['dermveda:title'];
                    subArticle.introduction = val.data.items['dermveda:introduction'];
                    if (val.data.items['dermveda:mainimage'] != 'undefined') {
                        subArticle.mainImageUrl = val.data.items['dermveda:mainimage'];
                    }

                    subArticle.id = val.data['id'];
                    subArticle.taxonomy = val.data.items['hippotaxonomy:canonkey'];
                    subArticle.tags = val.data.items['hippostd:tags'][0];
                    subArticles.push(subArticle);
                });
            });
        });
        articleSection.sub = subArticles;
        return articleSection.sub;
    }

    var featuredDocument = function(tag) {
        var featureditem = {};
        var featured = {};
        $http.get(config.apiUrl + '?_query=' + tag)
            .then(function(response) {
                angular.forEach(response.data.items, function(val, key) {
                    featureditem.url = val.link.url;
                })
                if (featureditem.url) {
                    $http.get(featureditem.url).then(function(response) {
                        featured.title = response.data.items['dermveda:title'];
                        featured.introduction = response.data.items['dermveda:introduction'];
                        featured.id = response.data['id'];
                    });
                }
            })
        return featured;
    }

    var FAQ = function(tag) {
        var faqs = [];
        var promisses = [];
        $http.get(config.apiUrl + '?_query=' + tag).then(function(res) {
            angular.forEach(res.data.items, function(val, key) {
                if (val.link.url) {
                    promisses.push($http({
                        method: 'GET',
                        url: val.link.url
                    }));
                }
            });
            $q.all(promisses).then(function(results) {
                angular.forEach(results, function(val, key) {
                    var faq = {};
                    faq.title = val.data.items['dermveda:string'];
                    faq.Question = val.data.items['dermveda:hippostd_html_']['content'];
                    faq.Qanswer = val.data.items['dermveda:hippostd_html__']['content'];
                    faq.author = val.data.items['dermveda:string_'];
                    faq.avatar = val.data.items['dermveda:hippogallerypicker_imagelink']['link']['url'];
                    faqs.push(faq);
                });

            });
        });
        return faqs;
    }

    return {
        documents: documents,
        featuredDocument: featuredDocument,
        FAQ: FAQ,
        herbalCorner: herbalCorner
    }

}]);
