dermvedaApp.factory('urlService', function($resource, config, $http, $location) {
    return {
        navigate: function(id) {
            switch (id) {
                case "7b1e6ca3-ab7f-4914-96c9-f515b7f3fc7c":
                    $location.path('/ourstory');
                    return;
                case "a0a7fc4b-a729-431e-95d7-75a1006b9db5":
                    $location.path('/privacy');
                    return;
                case "68442836-0c51-480f-8390-8a526a55d634":
                    $location.path('/terms');
                    return;
                case "4904b9a4-83b6-417d-bea1-a4b5eb59c719":
                    $location.path('/disclaimer');
                    return;
            }
            $http.get(config.apiUrl + id).then(function successCallback(response) {
                var canonKey = response.data.items['hippotaxonomy:canonkey'],
                    name = response.data.name;
                if (typeof canonKey !== 'undefined' && typeof name !== 'undefined' && canonKey !== '' && name !== '') {
                    $location.path('/' + canonKey + '/' + name);
                } else {
                    $location.path('/404');
                }
            });
        }
    }
});
