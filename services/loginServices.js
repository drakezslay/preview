dermvedaApp.factory('loginServices', ['$http', 'config', '$q','$location', function ($http, config, $q, $location) {

        return {
            resetPassword: function () {
                var email = $("#forget_email").val();

                if (email == "") {
                    console.log("fail");
                    $('#fail').fadeIn();
                    window.setTimeout(function () {
                        $("#fail").alert('close');
                    }, 800);
                    return;
                }
                $http.post(config.apiUrlWebService + 'dermveda1/send-reset-mail/?email=' + email + '&from=info@dermveda.com').success(function(response){
                  if (response.status == "200") {
                      $("#resetError").html("A password reset link has been emailed to you.");
                  } else {
                      $("#resetError").html("Email address not found. <u><a href='#/signup'>Click here</a></u> to sign up");
                  }
                });
            },
            resetPass: function () {
                var token = $("#token").val();
                var confirmPass = $("#newPass").val();
                $.ajax({
                    method: 'POST',
                    type: 'POST',
                    url: config.apiUrl + 'dermveda1/reset-password/' + token,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==");
                    },
                     headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='
                    },
                    success: function (response) {
                        if (response.status == "200") {
                            $.ajax({
                                method: 'POST',
                                type: 'POST',
                                url: config.apiUrl + 'dermveda1/update-password/',
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization", "Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==");
                                },
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='
                                },
                                data: '{"username": "' + response.useremail + '", "password": "' + confirmPass + '"}',
                                success: function (response) {
                                    if (response.status == "200") {
                                        if (window.localStorage) {
                                            window.localStorage.setItem('user', response);
                                            localStorage.setItem('profilename', response.result.firstName + " " + response.result.lastName);
                                            localStorage.setItem('profilefirstname', response.result.firstName);
                                            localStorage.setItem('profilelastname', response.result.lastName);
                                            localStorage.setItem('profileemail', response.result.email);
                                            localStorage.setItem('profileyob', response.result.yob);
                                            localStorage.setItem('profileinterests', response.result.interests);
                                            localStorage.setItem('profilegender', response.result.gender);
                                        }

                                        var redirectUrl = "http://www.dermveda.com/web/#/skin-type-profiler";

                                        if (routeParams.redirect) {
                                            redirectUrl = "http://www.dermveda.com/web/#/" + routeParams.redirect.replace(";", "/");
                                        }

                                        window.location.href = redirectUrl;
                                    } else {
                                        swal('', 'Invalid username or password', 'error');
                                    }
                                }
                            });
                        }
                    }
                });
            },
            userlogin : function (routeParams){
                var username = $("#loginformdivbottom input[name=username]").val();
                var password = $("#loginformdivbottom input[name=password]").val();

                $.ajax({
                    method: 'POST'
                    , type: 'POST'
                    , url: config.apiUrl + 'dermveda1/signin',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==");
                    }
                    , headers: {
                        'Content-Type': 'application/json'
                        , 'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='
                    }
                    , data: '{"encodeEnable": "true", "username": "' + username + '", "password": "' + password + '"}'
                    , success: function (response) {
                        if (response.status == "200") {
                            if (window.localStorage) {
                                window.localStorage.setItem('user', response);
                                localStorage.setItem('profilename', response.result.firstName + " " + response.result.lastName);
                                localStorage.setItem('profilefirstname', response.result.firstName);
                                localStorage.setItem('profilelastname', response.result.lastName);
                                localStorage.setItem('profileemail', response.result.email);
                                localStorage.setItem('profileyob', response.result.yob);
                                localStorage.setItem('profileinterests', response.result.interests);
                                localStorage.setItem('profilegender', response.result.gender);
                            }

                            var redirectUrl = "#/skin-type-profiler";

                            if(routeParams.redirect){
                                redirectUrl = "#/" + routeParams.redirect.replace(";", "/");
                            }
                            window.location.href = redirectUrl;
                        } else {
                            swal('', 'Invalid username or password', 'error');
                        }
                    }
                });
            }
        };

    }]);
