dermvedaApp.factory('DocumentsService', function($resource, config) {
    return {
        getList: function(offset, max, query) {
            return $resource(config.apiUrl, {
                _offset: offset,
                _max: max,
                _query: query
            }).get();
        },
        getDocumentById: function(uuid) {
            return $resource(config.apiUrl + uuid).get();
        }
    }
});


dermvedaApp.factory('SkinProfiler', function() {
    return {
        setAnswers: function(data) {
            window.localStorage.setItem('skinprofileAnswers', data);
        },
        getAnswers: function() {
            return window.localStorage.getItem('skinprofileAnswers');
        },
        clearAnswers: function() {
            window.localStorage.setItem('skinprofileAnswers', "");
        }
    };
});
