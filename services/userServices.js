dermvedaApp.factory('User',['config', function(config) {

   var isLoggedIn = false;
   return {
     isLoggedIn: function() {
        if(window.localStorage){
            if(window.localStorage.getItem('user') != null && window.localStorage.getItem('user') != ""){
                isLoggedIn = true;
                $("#signupContainer").hide();
                $("#profileContainer").show();
            } else {
                isLoggedIn = false;
                $("#signupContainer").show();
                $("#profileContainer").hide();
            }
        }
        return isLoggedIn;
     },
     setUserData: function(data) { window.localStorage.setItem('user', data) },
     update: function() {
        if(window.localStorage){
            if(window.localStorage.getItem('user') != null && window.localStorage.getItem('user') != ""){
                isLoggedIn = true;
                var profilename = window.localStorage.getItem('profilename');
                $("#signupContainer").hide();
                $("#profileContainer").show();
            } else {
                isLoggedIn = false;
                $("#signupContainer").show();
                $("#profileContainer").hide();
            }
        }
    }
   };
}]);
