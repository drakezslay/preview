

function loadFunctions() {

  // Smooth Scroller Srart
  var $window = $(window);    //Window object
  
  var scrollTime = 0.8;     //Scroll time
  var scrollDistance = 300;   //Distance. Use smaller value for shorter scroll and greater value for longer scroll
    
  $window.on("mousewheel DOMMouseScroll", function(event){
    
    event.preventDefault(); 
                    
    var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
    var scrollTop = $window.scrollTop();
    var finalScroll = scrollTop - parseInt(delta*scrollDistance);
      
    TweenMax.to($window, scrollTime, {
      scrollTo : { y: finalScroll, autoKill:true },
        ease: Power1.easeOut, //For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
        autoKill: true,
        overwrite: 5              
      });
          
  });
  // Smooth Scroller End


$('html').on('click', function(e) {
  if (typeof $(e.target).data('original-title') == 'undefined' &&
     !$(e.target).parents().is('.popover.in')) {

    $('.popover').hide();
   $('.usersiginbtn').removeClass( "usersiginbtnactive" );

  }
});


// forget password show hide
$(document).on("click", "#forgetpasslink", function() {
  $("#loginformdiv").fadeOut(1);
  $("#forgetpassdiv").delay( 1 ).fadeIn(1);
 });
 $(document).on("click", "#loginformlink", function() {
  $("#forgetpassdiv").fadeOut(1);
  $("#loginformdiv").delay( 1 ).fadeIn(1);
 });

// Login Button Active status
$('.usersiginbtn').on('click', function(event) {

if($('.popover').hasClass('in')){
  $('.usersiginbtn').addClass( "usersiginbtnactive" );
} else {
  $('.usersiginbtn').removeClass( "usersiginbtnactive" );
}

});

// Site font controler
$("#mediumfont").addClass('mediundefault');

    $( "#smallfont" ).click(function() {
      $("body").addClass('smallbodyfont');
       $("body").removeClass('mediumbodyfont');
        $("body").removeClass('largebodyfont');
        $("#mediumfont").removeClass('mediundefault');

    });
    $( "#mediumfont" ).click(function() {
      $("body").addClass('mediumbodyfont');
      $("body").removeClass('smallbodyfont');
        $("body").removeClass('largebodyfont');
        $("#mediumfont").removeClass('mediundefault');
    });
    $( "#largefont" ).click(function() {
      $("body").addClass('largebodyfont');
       $("body").removeClass('mediumbodyfont');
        $("body").removeClass('smallbodyfont');
        $("#mediumfont").removeClass('mediundefault');
    });

// Scroll Control click and open accodian
      $("#accodianpan01btn").on("click", function() {
    $("#collapse1").collapse('show');
});
            $("#accodianpan02btn").on("click", function() {
    $("#collapse2").collapse('show');
});
      $("#accodianpan03btn").on("click", function() {
    $("#collapse3").collapse('show');
});
      $("#accodianpan04btn").on("click", function() {
    $("#collapse4").collapse('show');
});

 $("#accodianpan05btn").on("click", function() {
    $("#collapse5").collapse('show');
});



$('.sitecontentaccodian').on('show', function (e) {
         $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
    });

    $('.sitecontentaccodian').on('hide', function (e) {
        $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
    });

// Scroll Control
 $(".scrollermenu a[href^='#']").on('click', function(e) {
       e.preventDefault();

       var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
               if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top - 300
                }, 1000);
                return false;

            }
    });

    $(".homescrollermenu a[href^='#']").on('click', function(e) {
       e.preventDefault();

       var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
               if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top - 100
                }, 200);
                return false;


            }
    });
 
// Fullscreen Control
    $('#fullscreenicon').on('click', function(event) {
               $('.sitesidebar, .maintopbarwrap').fadeToggle(1000);
               $('.sitecontentsec').toggleClass( "sitefullwidth" );
    });

     $("div.bhoechie-tab-menu>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    $("div.bhoechie-tab-menu01>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab01>div.bhoechie-tab-content01").removeClass("active");
        $("div.bhoechie-tab01>div.bhoechie-tab-content01").eq(index).addClass("active");
    });
    $("div.bhoechie-tab-menu02>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab02>div.bhoechie-tab-content02").removeClass("active");
        $("div.bhoechie-tab02>div.bhoechie-tab-content02").eq(index).addClass("active");
    });
    $("div.bhoechie-tab-menu03>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab03>div.bhoechie-tab-content03").removeClass("active");
        $("div.bhoechie-tab03>div.bhoechie-tab-content03").eq(index).addClass("active");
    });
// hover mega menu
  $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);

    });

// Readmore Control
    $('.collapse').on('shown.bs.collapse', function(){

      $("#btnShowMore").click(function () {
            $('#readcont').fadeIn(1000);
            $('#btnShowMore').fadeOut(1000);
      });

      }).on('hidden.bs.collapse', function(){

      $('#readcont').fadeOut();
      $('#btnShowMore').fadeIn();
      });



            function buttonUp(){
                var inputVal = $('.searchbox-input').val();
                inputVal = $.trim(inputVal).length;
                if( inputVal !== 0){
                    $('.searchbox-icon').css('display','none');
                } else {
                    $('.searchbox-input').val('');
                    $('.searchbox-icon').css('display','block');

                }
            }

// Accodian icon change
      $('.collapse').on('shown.bs.collapse', function(){
      $(this).parent().find(".fa-angle-left").removeClass("fa-angle-left").addClass("fa-angle-down");
      $(this).parent().find(".accodianpanel").addClass("accodianpanelblue");

      }).on('hidden.bs.collapse', function(){
      $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-left");
      $(this).parent().find(".accodianpanel").removeClass("accodianpanelblue");

    });



// Home icon Hover
      $("img.original").hover(
function() {
$(this).stop().animate({"opacity": "0"}, "slow");
},
function() {
$(this).stop().animate({"opacity": "1"}, "slow");
});




 $(".viewAnswerbtn").on('click',function () {
            $(this).fadeOut(1);
            $(this).parent('li').find('.answer').fadeIn(1);
            $(this).parent('li').find(".hideAnswerbtn").removeClass('hidden');
            $(this).parent('li').find('.hideAnswerbtn').fadeIn(1);
      });

    $(".hideAnswerbtn").on('click',function () {
        $(this).fadeOut(1);
        $(this).parent('li').find('.answer').fadeOut(1);
        $(this).parent('li').find('.viewAnswerbtn').fadeIn(1);
    });




   $( document.body ).on( 'click', '.filterdropdown li', function( event ) {

      var $target = $( event.currentTarget );

      $target.closest( '.filterbtngroup' )
         .find( '[data-bind="label"]' ).text( $target.text() )
            .end();
   //       .children( '.filterdropdown-toggle' ).dropdown( 'toggle' );
      $(".filterbtngroup").removeClass("open");

      $target.closest("button").attr("aria-expanded", false);
      return false;

   });
   // Catogory pages image on off function
   $("#imageoff").on("click", function() {
    $(".sitecontentaccodianfaq .quesImg ").fadeOut(300);
    $("#imageoff").addClass("activelink");
    $("#imageon").removeClass("activelink");

});
      $("#imageon").on("click", function() {
    $(".sitecontentaccodianfaq .quesImg ").fadeIn(300);
     $("#imageoff").removeClass("activelink");
     $("#imageon").addClass("activelink");
});

}

function shareOnSocial(socialTag) {
  switch (socialTag) {
    case "fb":
      window.open("http://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(window.location.href) + "/");
      break;
    case "li":
      window.open("http://www.linkedin.com/shareArticle?mini=true&url=" + window.location.href);
      break;
    case "gp":
      window.open("https://plus.google.com/share?url=" + window.location.href);
      break;
    case "twi":
      window.open("https://twitter.com/share?url=" + window.location.href);
      break;
    case "mail":
      window.open("mailto:?subject=Dermveda&body=" + window.location.href);
      break;
  }
}
