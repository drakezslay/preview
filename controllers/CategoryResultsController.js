'use strict';

dermvedaApp.controller('CategoryResultsController', function($scope, $http, $routeParams, User, config) {

    $scope.resultTitle = $routeParams.category;
    $scope.showImages = true;
    $scope.resultsPerPage = 10;
    $scope.from = 0;
    $scope.to=$scope.resultsPerPage;
    $scope.pages = new Array();
    $scope.currentPage = 0;

    $scope.predicate = 'pubwfCreationDate';
    $scope.rev = true;

    $scope.setAZFilter = function(value) {
        $scope.rev = false;
        $scope.predicate = value;
    }

    $scope.setMostRecentFilter = function(value) {
        $scope.predicate = value;
        $scope.rev = true;
    }

    $scope.replaceDash = function(str) {
        return str.replace(new RegExp("-", "g"), ' ');
    }

    $scope.chagePage = function(action) {
        switch(action) {
            case "next":
                $scope.currentPage += 1;
                if($scope.currentPage != 0) {
                    $scope.from = $scope.currentPage * $scope.resultsPerPage;
                } else {
                    $scope.from = $scope.resultsPerPage;
                }
                $scope.to = $scope.from + $scope.resultsPerPage;

                break;
            case "prev":
                $scope.currentPage -= 1;
                if($scope.currentPage != 0) {
                    $scope.from = $scope.currentPage * $scope.resultsPerPage;
                } else {
                    $scope.from = 0;
                }
                $scope.to = $scope.from + $scope.resultsPerPage;

                break;
            default:
                $scope.currentPage = action;
                $scope.from = (action-1) * $scope.resultsPerPage;
                $scope.to = $scope.from + $scope.resultsPerPage;
                break;
        }
    };

    $http.get(config.apiUrlWebService + "dermveda1/get-docs?key=" + $scope.resultTitle).then(function successCallback(response) {
        if(response.data.status == 200) {
            $scope.resultItems = JSON.parse(response.data.result);
            for(var i=1; i <= $scope.resultItems.length/$scope.resultsPerPage; i++) {
                $scope.pages.push(i);
            }
        }
      });
      
    User.update();
});
