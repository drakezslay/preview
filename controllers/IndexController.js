'use strict';

dermvedaApp.controller('DocumentsController', ['$scope', '$http', '$routeParams', 'DocumentsService', 'User', 'config', '$q', '$timeout','$location','$rootScope', function ($scope, $http, $routeParams, DocumentsService, User, config, $q, $timeout, $location,$rootScope) {
        $scope.mostReads = [];

        $scope.questionFormTitle = 'DO YOU HAVE A QUESTION ABOUT SKIN? WE WANT TO HEAR IT!';
        loadFunctions();
        $scope.relatedArticles = [];
        $scope.isFavoriteItem = false;
        if (!$routeParams.uuid) {

            $scope.currentPage = 1;
            $scope.itemsPerPage = 6;
            $scope.query = '';

            $http.get('config.php')
                .success(function (data) {
                    $scope.phpdata = data;
                })
                .error(function (err) {
                    $scope.phpdata = err;
                });

            $scope.update = function ($scope) {
                $scope.offset = ($scope.currentPage - 1) * $scope.itemsPerPage;
                DocumentsService.getList($scope.offset, $scope.itemsPerPage, $scope.query).$promise.then(function (response) {
                    $scope.documents = response;
                    $scope.totalItems = $scope.documents['total'];
                });
            }

            $scope.update($scope);

            $scope.pageChanged = function () {
                $scope.update($scope);
            };

            $scope.search = function () {
                $scope.update($scope);
            }

        } else {
            DocumentsService.getDocumentById($routeParams.uuid).$promise.then(function (response) {
                if(response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id'] === "7b1e6ca3-ab7f-4914-96c9-f515b7f3fc7c") {
                    response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id']='#/ourstory';
                 } else if(response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id'] === undefined) {
                    response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id'] = "";
                 } else {
                    response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id'] = "#/" + response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id']
                 }

                 if(response.items['dermveda:editedby']['items']['dermveda:link']['link']['id'] === "7b1e6ca3-ab7f-4914-96c9-f515b7f3fc7c") {
                    response.items['dermveda:editedby']['items']['dermveda:link']['link']['id']='#/ourstory';
                 } else if(response.items['dermveda:editedby']['items']['dermveda:link']['link']['id'] === undefined) {
                    response.items['dermveda:editedby']['items']['dermveda:link']['link']['id'] = "";
                 } else {
                    response.items['dermveda:editedby']['items']['dermveda:link']['link']['id'] = "#/" + response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id']
                 }

                 if(response.items['dermveda:reviewedby']['items']['dermveda:link']['link']['id'] === "7b1e6ca3-ab7f-4914-96c9-f515b7f3fc7c") {
                    response.items['dermveda:reviewedby']['items']['dermveda:link']['link']['id']='#/ourstory';
                 } else if(response.items['dermveda:reviewedby']['items']['dermveda:link']['link']['id'] === undefined) {
                    response.items['dermveda:reviewedby']['items']['dermveda:link']['link']['id'] = "";
                 } else {
                    response.items['dermveda:reviewedby']['items']['dermveda:link']['link']['id'] = "#/" + response.items['dermveda:author'][0]['items']['dermveda:link']['link']['id']
                 }
                var payload = {};
                payload.articleId = $routeParams.uuid;
                $http.get(config.apiUrlWebService+'dermveda1/add-read',{params:payload}).then(function(response){

                });
                $scope.mostReadArticles(response.items['hippotaxonomy:canonkey']);
                $scope.getAtoZarticles(response.items['hippotaxonomy:canonkey']);
                $scope.document = response;
                $rootScope.title = $scope.document.items['dermveda:title'];
                var accordArr = new Array();
                var faqArr = new Array();
                var relatedArticlesRequests = [];

                angular.forEach(response.items['relateddocs:docs']['related'], function (val, key) {
                    relatedArticlesRequests.push($http({method: 'GET', url: val.link.url}));
                });
                $q.all(relatedArticlesRequests).then(function (result) {
                    angular.forEach(result, function (val, key) {
                        $scope.relatedArticles.push(val.data);
                    });
                });

                $.each(response.items, function (i, val) {
                    if (i == "dermveda:accordion") {
                        $.each(val, function (j, accord) {
                            var obj = {content: "", title: "", iconclass: "", author: ""};

                            $.each(accord.items, function (k, acc) {
                                if (k == "dermveda:content") {
                                    $.each(acc, function (o, cont) {
                                        if (o == "content") {
                                            obj.content = $("<div></div>").html(cont).html();
                                            obj.content = obj.content.replace(new RegExp("<a", 'g'), "<a class='content-link'");
                                        }

                                        if (o == "links") {
                                            $.each(cont, function (m, link) {
                                                obj.content = obj.content.replace(new RegExp(m, 'g'), link.url);
                                                obj.content = obj.content.replace(new RegExp("data-hippo-link", 'g'), "src");
                                            });
                                        }
                                    });
                                }
                                if (k == "dermveda:title") {
                                    obj.title = acc;
                                    obj.iconclass = acc.toLowerCase().replace(new RegExp(" ", 'g'), "");
                                }
                            });
                            if (obj.title != "" || obj.content != "") {
                                accordArr.push(obj);
                            }
                        });
                    }

                    if (i == "dermveda:faq") {
                        $.each(val, function (j, faq) {
                            var faqObj = {answer: "", question: ""};

                            $.each(faq.items, function (t, faqO) {
                                if (t == "dermveda:question") {
                                    faqObj.question = faqO;
                                }
                                if (t == "dermveda:answer") {
                                    faqObj.answer = faqO;
                                }
                            });
                            if (faqObj.question != "" && faqObj.answer != "") {
                                faqArr.push(faqObj);
                            }
                        });
                    }

                });
                $scope.accordions = accordArr;
                $scope.faqs = faqArr;

                $scope.content = $scope.resolveLinks(response);

            },
            function(error){
              $location.path('/404');
            });

            User.update();
        }

        $scope.replaceDash = function(str) {
            return str.replace(new RegExp("-", "g"), ' ');
        }
        $scope.mostReadArticles = function(id){
            var payload = {};
            payload.taxonomy = id;
            $http.get(config.apiUrlWebService+'dermveda1/most-read',{params:payload}).then(function(res){
                $scope.mostReads = res.data.result;
            });
        };

        $scope.getAtoZarticles = function(id){
            var payload = {};
            payload.taxonomy = id;
            $http.get(config.apiUrlWebService+'dermveda1/a-to-z-by-taxonomy',{params:payload}).then(function(res){
                $scope.AtoZarticles = res.data.result;
            });
        };

        $scope.isArticleFavouriteItem = function () {
            if (localStorage.getItem('profileemail')) {
                var payload = {};
                payload.email = localStorage.getItem('profileemail');
                payload.article_id = $routeParams.uuid;
                payload = JSON.stringify(payload);
                $http.post(config.apiUrlWebService + 'dermveda1/is-favorite', payload).then(function (data, status) {
                    if (data.data.result) {
                        $scope.isFavoriteItem = true;
                    }
                });
            }
        };

        $scope.isArticleFavouriteItem();

        $scope.addFavourite = function (articleID) {
            var payload = {};
            if (localStorage.getItem('profileemail')) {
                payload.email = localStorage.getItem('profileemail');
                payload.articleid = articleID;
                $http.get(config.apiUrlWebService + 'dermveda1/favorite-article', {params: payload}).then(function (respose) {
                    if (respose.status === 200) {
                        $scope.isFavoriteItem = true;
                        $scope.msg = 'Article added to favorites successfully';
                    } else {
                       $scope.msg = 'Failed to add article to favorites';
                    }
                    $('#addfavmodalsucces').modal('show');
                });
            } else {
                $('#addfavmodal').modal('show');
            }
        };

        $scope.openSigninForm = function(){
            $('#addfavmodal').modal('hide');

            $timeout(function(){
                $("#loginformclick").popover('show');
                 window.scrollTo(0, 0);
            },300);
        };

        $scope.navigateToSignUpForm = function(){
            $('#addfavmodal').modal('hide');
            $location.path('/signup');
        }

         $scope.removeFavorite = function(id){



        if(localStorage.getItem('profileemail')){
            var payload = {};
            payload.email = localStorage.getItem('profileemail');
            payload.article_id = id;
            payload = JSON.stringify(payload);
            $http.post(config.apiUrlWebService+'/dermveda1/unfavorite-articles', payload).then(function(result, status){
                if(result.data.status == "200"){
                    $scope.isFavoriteItem = false;
                    $scope.msg = "Article successfully removed from favorites";
                } else {
                    $scope.msg = "Error occured while removing article from favorites";
                }
                $('#addfavmodalsucces').modal('show');
            });
        };
    };



        $scope.clickTrigger = function (obj) {
            $("#" + obj.accord.iconclass).slideToggle();
        };

        $scope.clickTriggerIcon = function (obj, slide) {
            $("#" + obj.accord.iconclass).slideDown();
            if (slide) {
                $('html,body').animate({scrollTop: $("#" + obj.accord.iconclass).offset().top - 350}, 'slow');
            }
        };



        $scope.clickRefTrigger = function (obj) {
            $("#" + obj).slideToggle();
        };

        $scope.goToByScroll = function (id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top}, 'slow');
        }

        $scope.goToFaq = function (id) {
            $('html,body').animate({scrollTop: $("#accodianpan05").offset().top - 300}, 'slow');

            if ($("#collapse5 .panel-body").is(':visible') == false) {
                $("#collapse5").slideDown();
            }
        }

        $scope.resolveLinks = function (response) {
            var someElement = document.createElement('div');
            someElement.innerHTML = response.items['dermveda:content'].content;
            var links = someElement.querySelectorAll('[data-hippo-link]');
            for (var index = 0; index < links.length; index++) {
                if (response.items['dermveda:content'].links[links[index].getAttribute('data-hippo-link')]) {
                    var uuid = response.items['dermveda:content'].links[links[index].getAttribute('data-hippo-link')].url;
                    $(links[index]).attr('src', uuid.replace(new RegExp(config.apiUrlWebService + 'rest/api/documents/', 'g'), '#/'));
                    $(links[index]).attr('href', uuid.replace(new RegExp(config.apiUrlWebService + 'rest/api/documents/', 'g'), '#/'));
                }
            }
            return someElement.innerHTML;

        };

        $("#accodianpan05").click(function () {
            $("#collapse5").slideToggle();
        });

        angular.element(document).ready(function () {
            $timeout(function () {

                if ($(window).width() < 768) {
                    $('.tabcaruncel').slick({
                        slidesToShow: 1,
                        arrows: true,
                        centerMode: true,
                        focusOnSelect: true
                    });

                } else {
                    $('.tabcaruncel').slick({
                        slidesToShow: 2,
                        arrows: true,
                        centerMode: true,
                        focusOnSelect: true
                    });
                }


                $('.tabcaruncel').slick('setPosition');
                var stickyNavTop = $('header').offset().top;
            }, 3500);

        });

    }]);


function htmlDecode(value) {
    return $('<div/>').html($('<div/>').html(value).text()).text();
}
