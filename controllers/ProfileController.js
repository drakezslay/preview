'use strict';

dermvedaApp.controller('ProfileController', ['$scope','User','config','$http','$location','$timeout','$rootScope',function ($scope, User, config, $http, $location, $timeout, $rootScope) {

    if (!localStorage.getItem('profileemail') && $location.path() === '/profile') {
        $location.path('/signup');
    }
    $scope.dataLoaded = false;
    $rootScope.$on('$locationChangeSuccess',function(evt, absNewUrl, absOldUrl) {
        var saveToProfile = localStorage.getItem('saveToProfile');
        if((absOldUrl.search("educator-symptoms/results") > -1 && absNewUrl.search('profile') > -1) || saveToProfile == 'educator')
        {
          setTimeout(function(){
            if($('#skin-symptoms-tab').length >0)
            {
              $('#home').removeClass('active in');
              $('#account-settings-tab').removeClass('active');
              $('#skin-symptoms-tab').addClass("active");
              $('#symptoms').addClass('active in');
            }
          }, 500);
        }

        if((absOldUrl.search("skin-type-profiler/results") > -1 && absNewUrl.search('profile') > -1) || saveToProfile == 'skinType')
        {
          setTimeout(function(){
            if($('#skin-types-tab').length >0)
            {
              $('#home').removeClass('active in');
              $('#account-settings-tab').removeClass('active');
              $('#skin-types-tab').addClass("active");
              $('#profile').addClass('active in');
            }
          }, 500);
        }
      });

    var image = window.localStorage.getItem('skinprofileImageData');
    var useremail = localStorage.getItem('profileemail');

    $scope.profile = {};
    $scope.favorites = [];
    $scope.years = [];

    for (var i=config.DOBStrat; i<config.DOBEnd+1; i++) {
      $scope.years.push(i);
    }
    User.update();

    $scope.loadFavorites = function () {
        if (localStorage.getItem('profileemail')) {
            var payload = {};
            payload.email = localStorage.getItem('profileemail');
            $http.get(config.apiUrlWebService + 'dermvedaapi/get-favorite-articles', {params: payload}).then(function (result, status) {
                if (result.status === 200) {
                    $scope.favorites = result.data.result;
                }
            });
        };
    }

    $scope.loadFavorites();

    $scope.removeFavorite = function (id) {
        if (localStorage.getItem('profileemail')) {
            var payload = {};
            payload.email = localStorage.getItem('profileemail');
            payload.article_id = id;
            payload = JSON.stringify(payload);
            $http.post(config.apiUrlWebService + '/dermvedaapi/unfavorite-articles', payload).then(function (result, status) {
                if (result.data.status == "200") {
                    $scope.loadFavorites();
                    swal('', 'Article successfully removed from favorites.', 'success');
                } else {
                    swal('', 'Error occured while removing article from favorites.', 'error');
                }
            });
        }
        ;
    };

    $scope.profile.interests = {};

    $scope.update = function () {
        var updateObj = {};
        updateObj.firstName = $scope.profile.firstname;
        updateObj.lastName = $scope.profile.lastname;
        updateObj.email = $scope.profile.email;
        updateObj.gender = $scope.profile.gender;
        updateObj.yob = $scope.profile.yob;
        updateObj.interests = JSON.stringify($scope.profile.interests);
        if ($scope.forfile.$valid) {
            $http.post(config.apiUrlWebService + 'dermvedaapi/update-user-profile-info', updateObj).then(function (response) {
                $scope.status = response.status;
                window.localStorage.setItem('user', response.data);
                localStorage.setItem('profilename', response.data.result.firstName + " " + response.data.result.lastName);
                localStorage.setItem('profilefirstname', response.data.result.firstName);
                localStorage.setItem('profilelastname', response.data.result.lastName);
                localStorage.setItem('profileemail', response.data.result.email);
                localStorage.setItem('profileyob', response.data.result.yob);
                localStorage.setItem('profileinterests', response.data.result.interests);
                localStorage.setItem('profilegender', response.data.result.gender);
            });
        }

    };

    $scope.loadProfileInfo = function(){
      var payload = {};
      payload.username = localStorage.getItem('profileemail');
      $http.post(config.apiUrlWebService + 'dermvedaapi/get-user-profile-info',payload).success(function(response){
        if(response.status == "200"){
          $scope.profile = {};
          $scope.profile.firstname = response.result.firstName;
          $scope.profile.lastname = response.result.lastName;
          $scope.profile.email = response.result.email;
          $scope.profile.confirmemail = response.result.email;
          $scope.profile.yob = response.result.yob;
          $scope.profile.password = null;
          $scope.profile.interests = JSON.parse(response.result.interests.replace(/^"(.*)"$/, '$1'));
          $scope.profile.gender = response.result.gender;
          $scope.profile.answers = response.result.answers;
          var skinIssues = JSON.parse(response.result.skin_issues);
          if (skinIssues != null) {
              $scope.profile.skinIssueObj = skinIssues;
              localStorage.setItem('profilePic', $scope.profile.skinIssueObj.imageURL);
              $rootScope.profileImage = $scope.profile.skinIssueObj.imageURL;
              $scope.profile.skinIssuesTab = true;
              $("#smallSkinImage img").attr("src", skinIssues.imageURL);
              $("#smallSkinImage img").attr("style", "border-radius:50%");
              $("#smallSkinImage img").attr("width", "127");
              $("#smallSkinImage img").attr("height", "127");
              $("#accmodal01 .modal-body img").attr("src", skinIssues.imageURL);
              $scope.hideImageProcessing = true;
          } else {
              $scope.profile.skinIssuesTab = false;
          }
          var skinConditions = JSON.parse(response.result.skin_condition);
          if (skinConditions != null) {
              $scope.profile.skinConditionsTab = true;
              var resAnswe = skinConditions.questionAnswers;
              var answers_1 = [];
              var answers_2 = [];
              var answers_3 = [];
              $.each(resAnswe, function (i, question) {
                  if (question.QuestionID == 1) {
                      $.each(question.questionAnswer, function (i, val) {
                          answers_1.push(val.AnswerText);
                      })
                  }
                  if (question.QuestionID == 2) {
                      $.each(question.questionAnswer, function (i, val) {
                          answers_2.push(val.AnswerText);
                      })
                  }
                  if (question.QuestionID == 3) {
                      $.each(question.questionAnswer, function (i, val) {
                          answers_3.push(val.AnswerText);
                      })
                  }
                  $scope.profile.answers_1 = answers_1;
                  $scope.profile.answers_2 = answers_2;
                  $scope.profile.answers_3 = answers_3;
              });
              var skindConditions = skinConditions.skinConditionsList;
              var skinConditionListArr = [];
              var skinConditionArr = [];
              var view = 0;
              $scope.profile.skinConditionList = [];
              if (skindConditions.length > 1) {
                  $.each(skindConditions, function (i, condition) {
                      i = i + 1;
                      if (i % 3 === 1) {
                          if (skinConditionArr.length === 3) {
                              skinConditionListArr.push(skinConditionArr);
                          }
                          skinConditionArr = [];
                      }
                      skinConditionArr.push(condition);
                      if (skindConditions.length === i) {
                          skinConditionListArr.push(skinConditionArr);
                      }
                  });
              } else {
                  skinConditionArr.push(skindConditions[0]);
                  skinConditionListArr.push(skinConditionArr);
              }
              $scope.profile.skinConditionListRemain = skinConditionListArr;
              for (var i=0; i<3 && $scope.profile.skinConditionListRemain.length > 0; i++){
                var condition = $scope.profile.skinConditionListRemain.shift();
                $scope.profile.skinConditionList.push(condition);
              }
          } else {
              $scope.profile.skinConditionsTab = false;
          }
          $scope.dataLoaded = true;
        }
      });
    }

    $scope.loadMore = function () {
        for (var i=0; i<3 && $scope.profile.skinConditionListRemain.length > 0; i++){
            var condition = $scope.profile.skinConditionListRemain.shift();
            $scope.profile.skinConditionList.push(condition);
        }
    };

    if($location.path() === '/profile'){
        var saveToProfile = localStorage.getItem('saveToProfile');
        if (saveToProfile == 'skinType') {
            
            localStorage.setItem('saveToProfile', '');
            var data = localStorage.getItem('skinTypeResult');
            var resultObj = JSON.parse(data);
            var userID = localStorage.getItem('profileemail');
            resultObj.userID = userID;
            localStorage.removeItem('skinTypeResult');
            var data = JSON.stringify(resultObj);
            $http({
                method: 'POST',
                data: data,
                url: config.apiUrlWebService + 'dermvedaapi/save-analysed-profiler-data',
                headers: {'Content-Type': 'application/json', 'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='}
            })
            .then(function (response) {
                $scope.loadProfileInfo();
            });
            
        } else if (saveToProfile == 'educator') {
            
            localStorage.setItem('saveToProfile', "");
            var data = localStorage.getItem('educatorResutl');
            var resultSkinCondition = JSON.parse(data);
            resultSkinCondition.userID = localStorage.getItem('profileemail');

            var data = JSON.stringify(resultSkinCondition);

            $http({
                method: 'POST',
                data: data,
                url: config.apiUrlWebService + 'dermvedaapi/save-analysed-educator-data',
                headers: {'Content-Type': 'application/json', 'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='}
            })
            .then(function (response) {
                $scope.loadProfileInfo();
            });
                    
        } else {
            $scope.loadProfileInfo();
        }
    }else{
        if(!localStorage.getItem('profilePic')){
            $scope.loadProfileInfo();
        }else{
           $rootScope.profileImage =  localStorage.getItem('profilePic');
        }
    }

    angular.element(document).ready(function () {
        $("#profiletab li a").click(function () {
            $("#content .tab-pane").removeClass("in active");
            $("#content #" + $(this).data("tab")).removeClass("skintabcontent");
            $("#content #" + $(this).data("tab")).addClass("in active");
            $("#content #" + $(this).data("tab")).show();
            $(".testClass").addClass("in active");
        });
        $(".medicinetypetabs ul li a").click(function () {
            var tab = $(this).attr("data-toggleTab");
            $("#tabContentEdit div").removeClass("in active");
            $("#" + tab).addClass("in active");
        });
        $("#profileimg").attr("style", "border-radius:50%");
        $("#profileimg").attr("width", "46");
        $("#profileimg").attr("height", "46");

        $("#profileimghover").attr("style", "border-radius:50%");
        $("#profileimghover").attr("width", "46");
        $("#profileimghover").attr("height", "46");

        $("#skinimgres").attr("width", "127");
        $("#skinimgres").attr("height", "127");
    });

}]);
