'use strict';

dermvedaApp.controller('TermsController', function($scope, User, DocumentsService) {
    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("68442836-0c51-480f-8390-8a526a55d634").$promise.then(function(response) {
    	console.log(response);
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == 'hippostd:html'){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });

    $scope.focusOnSubscribe = function(id) {
    	$("#"+id).focus();
    };
});
