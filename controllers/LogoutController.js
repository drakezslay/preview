'use strict';

dermvedaApp.controller('LogoutController', ['$scope','$rootScope', '$location', 'User', function ($scope, $rootScope, $location, User) {

    loadFunctions();

    window.localStorage.setItem('user', "");
    localStorage.setItem('profilename', "");
    localStorage.setItem('profilefirstname', "");
    localStorage.setItem('profilelastname', "");
    localStorage.setItem('profileemail', "");
    localStorage.setItem('profileyob', "");
    localStorage.setItem('profileinterests', "");
    localStorage.setItem('profilegender', "");
    localStorage.setItem('role', "");

    $("#adminLink").hide();

    User.update();

    $location.path( "/" );

    $rootScope.isLogged = false;

}]);
