'use strict';

dermvedaApp.controller('TagResultsController', function($scope, $http, $routeParams, User, config, $location, $rootScope, urlService) {
    $scope.search = {};
    $scope.pages = 0;
    $scope.entryLimit = 10;
    $scope.images = true;
    if ($routeParams.filter) {
        $scope.filter = $routeParams.filter;
    } else {
        $scope.filter = 'mr';
    }
    $scope.pageNumber = $routeParams.page;
    $scope.terms = decodeURIComponent($routeParams.terms);
    $scope.total = 0;
    $scope.searchBase = '#/tag/' + encodeURIComponent($scope.terms) + '/page/';
    $scope.imageURL = config.apiUrlWebService;

    $scope.loadSearchResults = function() {
        var payload = {};
        payload.page = $scope.pageNumber;
        payload.q = $scope.terms;
        payload.filter = $scope.filter;
        if ($routeParams.page && $routeParams.terms) {
            $http.get(config.apiUrlWebService + 'rest/api-manual/search', {
                params: payload
            }).then(function(response) {
                $scope.noresults = '';
                $scope.search = response.data.items;
                $scope.total = response.data.total;
                $scope.pages = Math.ceil(response.data.total / $scope.entryLimit);
                $rootScope.serchTerm = null;
                $rootScope.searchForm = {};
                if ($scope.total === 0) {
                    $scope.noresults = 'Your search -' + $scope.terms + ' - did not match any documents';
                }
            });
        }
    };

    $scope.urlService = urlService;

    $scope.loadSearchResults();

    $scope.getNumber = function(num) {
        return new Array(num);
    };

    $scope.firstPage = function() {
        if ($scope.total > $scope.entryLimit && $scope.pageNumber != 1) {
            $scope.pageNumber = 1;
            var url = '/search/' + encodeURIComponent($scope.terms) + '/page/' + $scope.pageNumber + '/filter/' + $scope.filter;
            $location.path(url);
        }
    };

    $scope.lastPage = function() {
        if ($scope.total > $scope.entryLimit && $scope.pageNumber != $scope.pages) {
            $scope.pageNumber = $scope.pages;
            var url = '/search/' + encodeURIComponent($scope.terms) + '/page/' + $scope.pageNumber + '/filter/' + $scope.filter;
            $location.path(url);
        }
    };

    $scope.filterResults = function(filter, label) {
        $rootScope.filterLable = label;
        $scope.filter = filter;
        $scope.pageNumber = 1;
        var url = '/search/' + encodeURIComponent($scope.terms) + '/page/' + $scope.pageNumber + '/filter/' + $scope.filter;
        $location.path(url);
    };

    $scope.deserialize = function(string) {
        return JSON.parse(string);
    };


   

    User.update();

    angular.element(document).ready(function() {
        loadFunctions();
        $('#srch-term').focus();
        $('.searchbox-input').focusout();
    });

});
