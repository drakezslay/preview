'use strict';

dermvedaApp.controller('HealthController', function($scope, User, $http, config, MetaService, $rootScope) {

    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Skin Conditions, Diseases, and Disorders | Dermveda", "Find reliable information about a variety of skin issues written by leading skincare experts. Learn about common skin infections and diseases.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";

    $scope.skinConditionSearchLetter = 'a';
    $scope.skinConditions = new Array();
    $scope.skinConditionsCurrentPage = 1;
    $scope.skinConditionsPerPage = 10;
    $scope.skinConditionFrom = 0;
    $scope.skinConditionTo = $scope.skinConditionsPerPage;
    $scope.skinConditionPages = new Array();

    $scope.changeSkinConditionPage = function(action, event) {
        switch(action) {
            case "next":
                if($scope.skinConditionsCurrentPage + 1 <= $scope.skinConditionPages.length) {
                    $scope.skinConditionsCurrentPage++;
                    $scope.skinConditionFrom = $scope.skinConditionTo;
                    $scope.skinConditionTo = $scope.skinConditionFrom + $scope.skinConditionsPerPage;
                }
                break;
            case "prev":
                if($scope.skinConditionsCurrentPage - 1 > 0) {
                    $scope.skinConditionsCurrentPage--;
                    $scope.skinConditionTo = $scope.skinConditionFrom;
                    $scope.skinConditionFrom = $scope.skinConditionTo - $scope.skinConditionsPerPage;
                }
                break;
            default:
                $scope.skinConditionsCurrentPage = action;
                $scope.skinConditionFrom = (action-1) * $scope.skinConditionsPerPage;
                $scope.skinConditionTo = $scope.skinConditionFrom + $scope.skinConditionsPerPage;
                break;
        }
    };

    $scope.changeSkinConditionLetter = function(letter) {
        retrieveSkinConditions(letter);
    };



    $scope.deepenKnowledgePerPage = 10;
    $scope.deepenKnowledgeItems;
    $scope.deepenKnowledgeCurrentPage = 1;
    $scope.deepenKnowledgeFrom = 0;
    $scope.deepenKnowledgeTo = $scope.deepenKnowledgePerPage;
    $scope.deepenKnowledgePages = new Array();

    $scope.changeKnowledgePage = function(action) {
        switch(action) {
            case "next":
                if($scope.deepenKnowledgeCurrentPage + 1 <= $scope.deepenKnowledgePages.length) {
                    $scope.deepenKnowledgeCurrentPage++;
                    $scope.deepenKnowledgeFrom = $scope.deepenKnowledgeTo;
                    $scope.deepenKnowledgeTo = $scope.deepenKnowledgeFrom + $scope.deepenKnowledgePerPage;
                }
                break;
            case "prev":
                if($scope.deepenKnowledgeCurrentPage - 1 > 0) {
                    $scope.deepenKnowledgeCurrentPage--;
                    $scope.deepenKnowledgeTo = $scope.deepenKnowledgeFrom;
                    $scope.deepenKnowledgeFrom = $scope.deepenKnowledgeTo - $scope.deepenKnowledgePerPage;
                }
                break;
            default:
                $scope.deepenKnowledgeCurrentPage = action;
                $scope.deepenKnowledgeFrom = (action-1) * $scope.deepenKnowledgePerPage;
                $scope.deepenKnowledgeTo = $scope.deepenKnowledgeFrom + $scope.deepenKnowledgePerPage;
                break;
        }
    };

    var retrieveDeepenYourKnowledgeItems = function() {
        $http.get(config.apiUrlWebService + "dermveda1/get-docs?key=deepen-your-knowledge").then(function successCallback(response) {
            if(response.data.status == 200) {
                $scope.deepenKnowledgeItems = JSON.parse(response.data.result);
                var pagesToFit = 0;
                if( $scope.deepenKnowledgeItems.length % $scope.deepenKnowledgePerPage > 0) {
                    pagesToFit = ($scope.deepenKnowledgeItems.length/$scope.deepenKnowledgePerPage) + 1;
                } else if ($scope.deepenKnowledgeItems.length < $scope.deepenKnowledgePerPage) {
                    pagesToFit = 0;
                } else{
                    pagesToFit = ($scope.deepenKnowledgeItems.length/$scope.deepenKnowledgePerPage);
                }
                for(var i=1; i <= pagesToFit; i++) {
                    $scope.deepenKnowledgePages.push(i);
                }

            }
          });
    }

    var retrieveSkinConditions = function(letter) {
        $scope.skinConditionSearchLetter = letter;
        $http.get(config.apiUrlWebService + "dermvedaapi/articles-by-letter?letter=" + letter).then(function successCallback(response) {

            if(response.data.status == 200) {
                $scope.skinConditions.length = 0;
                $scope.skinConditions = response.data.result;
                $scope.skinConditionPages.length = 0;

                $scope.skinConditionsCurrentPage = 1;
                $scope.skinConditionsPerPage = 10;
                $scope.skinConditionFrom = 0;
                $scope.skinConditionTo = $scope.skinConditionsPerPage;

                var pagesToFit = 0;
                if( $scope.skinConditions.length % $scope.skinConditionsPerPage > 0) {
                    pagesToFit = ($scope.skinConditions.length/$scope.skinConditionsPerPage) + 1;
                } else if ($scope.skinConditions.length <= $scope.skinConditionsPerPage) {
                    pagesToFit = 0;
                } else if($scope.skinConditions.length % $scope.skinConditionsPerPage == 0){
                    pagesToFit = ($scope.skinConditions.length/$scope.skinConditionsPerPage);
                }
                for(var i=1; i <= pagesToFit; i++) {
                    $scope.skinConditionPages.push(i);
                }
            }
          });
    }

    $scope.featuredGuests;

    var retrieveFeaturedGuests = function() {
        $http.get(config.apiUrlWebService + "dermveda1/sorted-by-id?taxonomy=featured-guests").then(function successCallback(response) {
            if(response.data.status == 200) {
                $scope.featuredGuests = response.data.result;
            }
          });
    }

    retrieveSkinConditions($scope.skinConditionSearchLetter);
    retrieveDeepenYourKnowledgeItems();
    retrieveFeaturedGuests();

    loadFunctions();
    User.update();
});
