'use strict';

dermvedaApp.controller('NavbarCtrl', function($scope, $location) {
    $scope.isActive = function(path){
        if(path === $location.path()){
            return true;
        }
        return false;
    };  
});
