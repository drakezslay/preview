'use strict';

dermvedaApp.controller('FeaturedGuestViewController', ['$scope', '$http', '$routeParams', 'DocumentsService', 'User', 'config', '$q', '$timeout','$location','$rootScope', function ($scope, $http, $routeParams, DocumentsService, User, config, $q, $timeout, $location,$rootScope) {
        $scope.title = ""

        DocumentsService.getDocumentById($routeParams.id).$promise.then(function (response) {
            $scope.title = response.displayName;
            $scope.imgSrc = response.items['dermveda:hippogallerypicker_imagelink'].link.url;
            $scope.content = response.items['dermveda:hippostd_html'].content;
        });
}]);
