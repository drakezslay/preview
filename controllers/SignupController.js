'use strict';

dermvedaApp.controller('SignupController', ['$scope', '$http', '$routeParams', '$localStorage', '$window', '$location', 'User', 'config','$timeout', function ($scope, $http, $routeParams, $localStorage, $window, $location, User, config, $timeout) {

    $scope.submitted = false;
    $scope.emailMatched = false;
    $scope.regData = {};
    $scope.regData.firstName = null;
    $scope.regData.lastName = null;
    $scope.regData.email = null;
    $scope.regData.confirmemail = null;
    $scope.regData.gender = null;
    $scope.regData.password = null;
    $scope.regData.yod = null;
    $scope.regData.interests = {};
    $scope.regData.agreeTerms = null;
    $scope.regData.agreed = null;
    $scope.infoMsg = false;
    $scope.newsLetter = true;
    var years = [];
    for (var i = config.DOBStrat; i < config.DOBEnd + 1; i++) {
      years.push(i);
    }
    $scope.years = years;

    $scope.userRegistration = function (model) {
        $scope.submitted = true;
        if (model.email == model.confirmEmail) {
            $scope.emailMatched = true;
        }
        if ($scope.signUpForm.$invalid == false && $scope.emailMatched) {
            if (model.agreed == false || model.agreed == null) {
                swal('Warning!', 'You have to agree to the terms and conditions', 'error');
            } else {
                model.interests = JSON.stringify(model.interests);
                if ($scope.newsLetter === true) {
                    $http.post(config.apiUrlWebService+'dermveda/mailchimp/subscribe', {email:$scope.regData.email})
                            .then(function (res){
                    });
                }
                $http.post(config.apiUrlWebService + 'dermvedaapi/signup', JSON.stringify(model)).
                   success(function (data, status, headers, config) {
                       if (data.status == "200" && data.result.isExsistingEmail == false) {
                           $scope.infoMsg = false;
                         if (window.localStorage) {
                               window.localStorage.setItem('user', data.result.response);
                               localStorage.setItem('profilename', data.result.response.firstName + " " + data.result.response.lastName);
                               localStorage.setItem('profilefirstname', data.result.response.firstName);
                               localStorage.setItem('profilelastname', data.result.response.lastName);
                               localStorage.setItem('profileemail', data.result.response.email);
                               localStorage.setItem('profileyob', data.result.response.yob);
                               localStorage.setItem('profileinterests', data.result.response.interests);
                               localStorage.setItem('profilegender', data.result.response.gender);
                           }
                           var redirectUrl = '';
                           if($routeParams.redirect){
                             redirectUrl = '/'+$routeParams.redirect.replace(';','/');
                           } else {
                             redirectUrl = "/profile";
                           }
                           console.log(redirectUrl);
                           $location.path(redirectUrl);
                       } else {
                           window.localStorage.setItem('user', "");
                           $scope.infoMsg = true;
                           $scope.errorMsg = "User regsitration failed. Please try again..";
                           $scope.errorMsg = data.result.response;
                           if (data.result.isExsistingEmail === true) {
                               $timeout(function () {
                                  $("#loginformclick").popover('show');
                                   window.scrollTo(150, 0);
                               }, 300);
                               $timeout(function () {
                                   $("#loginformdiv").css("display", "none");
                                   $("#forgetpassdiv").css("display", "block");
                                   window.scrollTo(150, 0);
                               }, 500);
                           }

                       }
                   }).
                    error(function (data, status, headers, config) {
                        window.localStorage.setItem('user', "");
                        $scope.infoMsg = true;
                        $scope.errorMsg = "User regsitration failed. Please try again..";
                        $scope.errorMsg = data.result.response;

                    });
            }

        } else {
            if ($scope.emailMatched == false)
                $scope.signUpForm.confirmEmail.$invalid = true;

        }
    }

    if ($routeParams.fname && $routeParams.lname && $routeParams.email) {
        $scope.regData.firstName = $routeParams.fname;
        $scope.regData.lastName = $routeParams.lname;
        $scope.regData.email = $routeParams.email;
        $scope.regData.confirmemail = $routeParams.email;
    }

        User.update();

        if (User.isLoggedIn()) {
            $location.path("/");
        }

    }]);
