'use strict';

var global_$scope = null;

dermvedaApp.controller('EducatorController', function ($q, $http, $scope, $location, User, SkinProfiler, config, MetaService, $rootScope, $window) {

    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Self Help Skin Symptom Checker | Dermveda", "Experiencing skin problems? Try our skin symptom educator and discover a variety of helpful articles, tips and resources related to your skin condition.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";

    global_$scope = $scope;

    User.update();

    $scope.saveQA = function () {
            if (!User.isLoggedIn()) {
                $scope.resultSkinCondition = {};
                $scope.resultSkinCondition.questionAnswers = resAnswe;

                $scope.resultSkinCondition.sessionID = $scope.sessionID;
                $scope.resultSkinCondition.skinConditionsList = $scope.skinConditionsList;
                var data = JSON.stringify($scope.resultSkinCondition);
                
                localStorage.setItem('saveToProfile', 'educator');
                localStorage.setItem('educatorResutl', data);
                $location.path("/educator-symptoms/register/profile");
            } else {
                $scope.resultSkinCondition = {};
                $scope.resultSkinCondition.userID = localStorage.getItem('profileemail');
                $scope.resultSkinCondition.questionAnswers = resAnswe;

                $scope.resultSkinCondition.sessionID = $scope.sessionID;
                $scope.resultSkinCondition.skinConditionsList = $scope.skinConditionsList;
                var data = JSON.stringify($scope.resultSkinCondition);

                $http({
                  method: 'POST',
                  data: data,
                  url: config.apiUrlWebService + 'dermvedaapi/save-analysed-educator-data',
                  headers: {'Content-Type': 'application/json','Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='},
                })
                .then(function(response){
                      $location.path( "/profile" );
                });
            }
        }

    $scope.populateEducatorResults = function (answersJSON) {
        var data = "{\"userID\": null, \"sessionID\": null, \"listOfAnswers\": " + answersJSON + ", \"skinCondition\": null}";

        $http({
          method:'POST',
          data:data,
          url:'https://www.dermveda.com/dermvedaapi/educator-profiler',
          headers: {'Content-Type': 'application/json','Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='},
        }).then(function(response){
          if (response.status = "200" && response.data.result !== null) {
              $scope.sessionID = response.data.result.sessionID;
              $scope.skinConditionsList = response.data.result.skinConditionsList;
              var skindConditions = response.data.result.skinConditionsList;

              var skinConditionListArr = [];
              var skinConditionListArrRemains = [];
              var skinConditionArr = [];
              var view = 1;

              if (true) {
                  angular.forEach(skindConditions, function (condition, i) {
                      if (view % 3)
                      {
                          condition.isThird = "FALSE";
                      } else
                      {
                          condition.isThird = "TRUE";
                      }
                      if (i < 9) {
                          skinConditionListArr.push(condition);
                      } else {
                          skinConditionListArrRemains.push(condition);
                      }
                      view++;
                  });
              }
              global_$scope.attempt = 1;
              global_$scope.skinConditionList = skinConditionListArr;
              global_$scope.skinConditionListRemain = skinConditionListArrRemains;
               
          }
        });

        var articleImgs = $("body").find(".proximalightfontfamily .educaterresultbox .divbg");
    }

    $scope.loadMore = function () {
        var count = 9;
        if (global_$scope.attempt === 1){
           var count = 9;
        }
        for (var i=0; i < count && global_$scope.skinConditionListRemain.length > 0; i++) {
            var conditon = global_$scope.skinConditionListRemain.pop();
          global_$scope.skinConditionList.push(conditon);
        }
        global_$scope.attempt  = global_$scope.attempt + 1;
    };

    $scope.locateSite = function(string) {
        var element = $("."+string.toLowerCase().replace("/",""));
        if(element.hasClass("hide")) {
            element.removeClass("hide")
        } else {
            element.addClass("hide")
        }
    }

    $scope.loadQuestionsAndAnswersEducator = function () {
        $http({
          method:'GET',
          headers:{'Content-Type': 'application/json','Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='},
          url: config.apiUrlWebService + 'dermvedaapi/get-face-skin-questions-educator'
        })
        .then(function(response){
          if (response.status = "200") {
              var question_1_1_answers = [];
              var question_1_2_answers = [];
              var question_1_3_answers = [];

              var question_2_1_answers = [];
              var question_2_2_answers = [];

              var question_3_1_answers = [];

              $scope.QuestionsAnswers = response.data.result;

              angular.forEach(response.data.result, function (question, i) {

                  if ((i + 1) == 1 && question.questionID == 1) {
                      var qsPerCol = question.questionAnswer.length/3;
                      if(qsPerCol != 0)
                          qsPerCol = Math.round(question.questionAnswer.length/3) + 1;
                      angular.forEach(question.questionAnswer, function (answer,j) {
                          if (answer.group == 'FEATURE') {
                              question_1_3_answers.push(answer);
                          } else if (answer.group == 'TEXTURE') {
                              question_1_2_answers.push(answer);
                          } else if (answer.group == 'COLOR') {
                              question_1_1_answers.push(answer);
                          }
                      });

                      $scope.question1_1Answers = question_1_1_answers;
                      $scope.question1_2Answers = question_1_2_answers;
                      $scope.question1_3Answers = question_1_3_answers;

                  } else if ((i + 1) == 2 && question.questionID == 2) {

                      var qsPerCol = question.questionAnswer.length/3 + 1;
                      if(qsPerCol != 0)
                          qsPerCol = Math.round(question.questionAnswer.length/3) + 1;

                      angular.forEach(question.questionAnswer, function (answer, j) {
                          if (j > qsPerCol) {
                              question_2_2_answers.push(answer);
                          } else if (j <= qsPerCol) {
                              question_2_1_answers.push(answer);
                          }
                      });

                      $scope.question2_1Answers = question_2_1_answers;
                      $scope.question2_2Answers = question_2_2_answers;

                  } else if ((i + 1) == 3 && question.questionID == 3) {
                      var tab = 1;
                      var position = {
                        "LOCAL" : "21px",
                        "DIFFUSE" : "-68px",
                        "LINEAR" : "-148px",
                        "MANY LESIONS" : "-309px",
                        "SINGLE LESION" : "-227px"
                      };
                      angular.forEach(question.questionAnswer, function (answer, j) {
                          answer.position = position[answer.answerText];
                          question_3_1_answers.push(answer);
                      });
                    $scope.question3_1Answers = question_3_1_answers;
                  }

              });

          }
        });
    }

    if ($location.absUrl().indexOf("results") != -1) {


        var answersJSON = window.localStorage.getItem('educatorAnswers');
        var resAnswe = JSON.parse(answersJSON);
        var answers_1 = [];
        var answers_2 = [];
        var answers_3 = [];
        angular.forEach(resAnswe, function (question, i) {
            if (question.questionID == 1) {
                angular.forEach(question.questionAnswer, function (val, i) {
                    answers_1.push(val.answerText);
                })
            }
            if (question.questionID == 2) {
                angular.forEach(question.questionAnswer, function (val, i) {
                    answers_2.push(val.answerText);
                })
            }
            if (question.questionID == 3) {
                angular.forEach(question.questionAnswer, function (val, i) {
                    answers_3.push(val.answerText);
                })
            }


            $scope.answers_1 = answers_1;
            $scope.answers_2 = answers_2;
            $scope.answers_3 = answers_3;

        });

        $scope.populateEducatorResults(answersJSON);



    } else {
        window.localStorage.setItem('skinprofileAnswers', "");
        window.localStorage.setItem('skinprofileImageData', "");
        window.localStorage.setItem('saveToProfile', '');

        $scope.ques1Answered = false;
        $scope.ques2Answered = false;

        $scope.onNextTab = function (current, next) {
            var shouldShow = true;

            angular.forEach($scope.QuestionsAnswers, function (question, i) {
                var objectQuestion = {questionID: "", questionText: "", questionAnswer: []};

                var checkText = "check1-";
                if (current == "collapse1")
                    checkText = "check1-";
                else if (current == "collapse02") {
                    checkText = "check2-";
                }

                try {
                    angular.forEach(question.questionAnswer, function (answer,i) {
                        var fieldId = checkText + answer.answerID;

                        if ($("#" + fieldId).is(":checked")) {
                            $(".alert").addClass("hidden");
                            $(".alert").hide();
                            $(".tablist .panel-group .panel .panel-collapse").slideUp();
                            $(".tablist .panel-group .panel #" + current).removeClass("in");
                            $(".tablist .panel-group .panel #" + current).parent().find(".panel-heading").addClass("accodianselected");
                            ;
                            $(".tablist .panel-group .panel #" + next).parent().find(".panel-heading a").removeClass("collapsed");
                            ;
                            $(".tablist .panel-group .panel #" + next).addClass("in");
                            $(".tablist .panel-group .panel #" + next).slideDown('fast', function(){
                                var position = $("#head_"+next).position();
                                var headerHight = $(".sticky").height();
                                $window.scrollTo(position.left,230);
                            });

                            if (current == "collapse1")
                                $scope.ques1Answered = true;
                            else if (current == "collapse02") {
                                $scope.ques2Answered = true;
                            }

                            shouldShow = false;
                            throw BreakException();
                        }
                    });
                } catch (e) {

                }
            });

            if (shouldShow) {
                $(".alert").removeClass("hidden");
                $(".alert").show();
                $('html,body').animate({scrollTop: $(".alert").offset().top - 100}, 'slow');
            }


        };

        $scope.onNextTab0 = function (current, next) {
            var shouldShow = true;

            angular.forEach($scope.QuestionsAnswers, function (question, i) {
                var objectQuestion = {questionID: "", questionText: "", questionAnswer: []};

                var checkText = "check1-";
                if (current == "collapse1")
                    checkText = "check1-";
                else if (current == "collapse02") {
                    checkText = "check2-";
                }

                try {
                    angular.forEach(question.questionAnswer, function (answer,i) {
                        var fieldId = checkText + answer.answerID;

                        if ($("#" + fieldId).is(":checked")) {
                            $(".alert").addClass("hidden");
                            $(".alert").hide();
                            $(".tablist .panel-group .panel .panel-collapse").slideUp();
                            $(".tablist .panel-group .panel #" + current).removeClass("in");
                            $(".tablist .panel-group .panel #" + current).parent().find(".panel-heading").addClass("accodianselected");
                            ;
                            $(".tablist .panel-group .panel #" + next).parent().find(".panel-heading a").removeClass("collapsed");
                            ;
                            $(".tablist .panel-group .panel #" + next).addClass("in");
                            $(".tablist .panel-group .panel #" + next).slideDown('fast', function(){
                                var position = $("#head_"+next).position();
                                var headerHight = $(".sticky").height();
                                $window.scrollTo(position.left,270);
                            });

                            if (current == "collapse1")
                                $scope.ques1Answered = true;
                            else if (current == "collapse02") {
                                $scope.ques2Answered = true;
                            }

                            shouldShow = false;
                            throw BreakException();
                        }
                    });
                } catch (e) {

                }
            });

            if (shouldShow) {
                $(".alert").removeClass("hidden");
                $(".alert").show();
                $('html,body').animate({scrollTop: $(".alert").offset().top - 100}, 'slow');
            }


        };

        $scope.switchTab = function (current, next) {
            var checkText = "check1-";
            if (current == "collapse1")
                checkText = "check1-";
            else if (current == "collapse02") {
                checkText = "check2-";
            }

            if(current === "collapse1" && next ==="collapse02"){
                $scope.onNextTab(current, next);
                return;
            }
            if(current === "collapse02" && next ==="collapse03"){
                $scope.onNextTab(current, next);
                return;
            }


            $(".alert").addClass("hidden");
            $(".alert").hide();
            $(".tablist .panel-group .panel .panel-collapse").slideUp();
            $(".tablist .panel-group .panel #" + current).removeClass("in");
            $(".tablist .panel-group .panel #" + next).parent().find(".panel-heading a").removeClass("collapsed");
            $(".tablist .panel-group .panel #" + next).addClass("in");
            $(".tablist .panel-group .panel #" + next).slideDown("fast", function(){
                var position = $("#head_"+next).position();
                var headerHight = $(".sticky").height();
                $window.scrollTo(position.left,position.top-headerHight);
            });

            angular.forEach($scope.QuestionsAnswers, function (question, i) {
                try {
                    angular.forEach(question.questionAnswer, function (answer, i) {
                        var fieldId = checkText + answer.answerID;
                        if ($("#" + fieldId).is(":checked")) {
                            if (current == "collapse1")
                                $scope.ques1Answered = true;
                            else if (current == "collapse02") {
                                $scope.ques2Answered = true;
                            }

                            throw BreakException();
                        }
                    });
                } catch (e) {
                }
            });
        };

        $scope.clickPatternItem = function ($event, isDropdown) {
            var educaterselectul = $(".educaterselectul").find("li span.arrowskinselecttext");
            var pattern = $($event.target).attr("data-val");
            var position;
            if(isDropdown) {
                pattern = $($event.target).attr('data-answertext');
                position = $($event.target).attr('data-position');
                $('.educaterselecterbg').css('background-position', position);
            }
            angular.forEach(educaterselectul, function (item, i) {
                if ($(item).data("val") == pattern) {
                    $(".educaterselectul li").removeClass("active");
                    $(item).parent().addClass("active");
                    $(".selectlabel").html(pattern);
                }
            });
        };


        $scope.loadQuestionsAndAnswersEducator();

        $scope.finishQA = function () {

            var questionAnswersArr = [];
            angular.forEach($scope.QuestionsAnswers, function(question, i){
                if((i+1) == 1){
                    var objectQuestion = { questionID: "", questionText: "", questionAnswer: []};

                    objectQuestion.questionID = question.questionID;
                    objectQuestion.questionText = question.questionText;

                    angular.forEach(question.questionAnswer, function (answer, i) {
                        var fieldId = "check1-" + answer.answerID;
                        if ($("#" + fieldId).is(":checked")) {
                            var objectQuestionAnswer = {questionID: "", questionText: "", answerID: "", answerText: ""};
                            objectQuestionAnswer.answerID = answer.answerID;
                            objectQuestionAnswer.answerText = answer.answerText;

                            objectQuestion.questionAnswer.push(objectQuestionAnswer);
                        }
                    });

                    questionAnswersArr.push(objectQuestion);
                }
                if ((i + 1) == 2) {
                    var objectQuestion = {questionID: "", questionText: "", questionAnswer: []};

                    objectQuestion.questionID = question.questionID;
                    objectQuestion.questionText = question.questionText;

                    angular.forEach(question.questionAnswer, function (answer, i) {
                        var fieldId = "check2-" + answer.answerID;
                        if ($("#" + fieldId).is(":checked")) {
                            var objectQuestionAnswer = {questionID: "", questionText: "", answerID: "", answerText: ""};
                            objectQuestionAnswer.answerID = answer.answerID;
                            objectQuestionAnswer.answerText = answer.answerText;
                            objectQuestion.questionAnswer.push(objectQuestionAnswer);
                        }
                    });

                    questionAnswersArr.push(objectQuestion);
                }
                var count = 0;
                if ((i + 1) == 3) {
                    try {
                        var objectQuestion = {questionID: "", questionText: "", questionAnswer: []};

                        objectQuestion.questionID = question.questionID;
                        objectQuestion.questionText = question.questionText;

                        angular.forEach(question.questionAnswer, function(answer, i){
                            var fieldId = "educaterselectul li.active span.arrowskinselecttext";
                            if($("."+fieldId).attr("data-val") == answer.answerText){
                                count++;
                                var objectQuestionAnswer = {questionID: "", questionText: "", answerID: "", answerText: ""};
                                objectQuestionAnswer.answerID = answer.answerID;
                                objectQuestionAnswer.answerText = answer.answerText;
                                objectQuestionAnswer.questionText = answer.questionText;
                                objectQuestionAnswer.questionId = answer.questionID;

                                objectQuestion.questionAnswer.push(objectQuestionAnswer);
                            }
                        });

                        questionAnswersArr.push(objectQuestion);
                    } catch (e) {

                    }
                }
            });

            window.localStorage.setItem('educatorAnswers', JSON.stringify(questionAnswersArr));
            $location.path("/educator-symptoms/results");
        };
    }

    $scope.$on('$viewContentLoaded', function() {
        $('.sitecontentaccodian').on('show', function (e) {
            $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        });

        $('.sitecontentaccodian').on('hide', function (e) {
            $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
        });

        $( document.body ).on( 'click', '.filterdropdown li', function( event ) {

            var $target = $( event.currentTarget );
            $target.closest( '.filterbtngroup' )
                .find( '[data-bind="label"]' ).text( $target.text() )
                .end();
            $(".filterbtngroup").removeClass("open");

            $target.closest("button").attr("aria-expanded", false);
            return false;
        });
    });

});
