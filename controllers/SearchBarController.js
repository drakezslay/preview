dermvedaApp.controller('SearchBarController', ['$scope', 'User', 'config','$compile','$routeParams','$rootScope', function ($scope, User, config, $compile,$routeParams,$rootScope) {

        $rootScope.searchForm = {};
        $scope.form = {};

        $rootScope.searchForm.searchItem = null;

        if($routeParams.terms){
            $scope.form.searchItemInner = decodeURIComponent($routeParams.terms);
        }

        $scope.keyPressed = function(keyEvent) {
            if (keyEvent.which === 13) {
                $scope.submitSearchBtn();
            }
        }

        $scope.submitSearch = function ($event) {
            if($rootScope.searchForm.searchItem && $rootScope.searchForm.searchItem.length > 0){
                var keyCode = $event.which || $event.keyCode;
                if(keyCode === 13){
                    url = "#/search/" + encodeURIComponent($rootScope.searchForm.searchItem) + "/page/1/" + "filter/mr";
                    var inputBox = $('.searchbox-input');
                    var searchBox = $('.searchbox');
                    inputBox.focusout();
                    inputBox.val('');
                    searchBox.removeClass('searchbox-open');
                    $('#searcwithcont').removeClass('addpadcontrol');
                    $(".sitesocialmediaicondiv, .signupmobile, .mobileserchhide").fadeIn(1000);

                    isOpen = false;
                    window.location.replace(url);
                };
            }
        };

        $scope.submitSearchBtn = function(){
            if($scope.middleForm.$valid && $scope.form.searchItemInner.length>0){
                url = "#/search/" + encodeURIComponent($scope.form.searchItemInner) + "/page/1/" + "filter/mr";
                window.location.replace(url);
            }
        };

        $scope.submitSearchBtnHeader = function(){
            if($rootScope.searchForm.searchItem && $rootScope.searchForm.searchItem.length > 0){
                url = "#/search/" + encodeURIComponent($rootScope.searchForm.searchItem) + "/page/1/" + "filter/mr"
                window.location.replace(url);
            }
        };

        angular.element(document).ready(function () {
            loadFunctions();

            $('body').on('click', function(){
                $rootScope.searchForm = {};
            });

        });
    }]);
