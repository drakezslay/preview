'use strict';

dermvedaApp.controller('ContactController', function($scope, User, DocumentsService) {
    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("d97217f0-f1a4-4ca0-a195-ce4e24c30b52").$promise.then(function(response) {
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == "hippostd:html"){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });
});
