'use strict';
dermvedaApp.controller('WellnessController', ['$scope', '$http', 'config', 'documentService', 'MetaService', '$rootScope', '$location', '$timeout', function($scope, $http, config, documentService, MetaService, $rootScope, $location, $timeout) {


    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Natural Health and Wellness Tips, Articles & Resources", "Improve your well-being and live a healthier lifestyle. Check out must-read diet, fitness and wellness articles written by leading health experts.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";

    $scope.featured = {};
    $scope.lifeStyle = {};
    $scope.dietAndNutrition = {};
    $scope.welnessFeaturedGuests = {};
    $scope.faqs = [];
    $scope.questionFormTitle = 'Do you have a question about WELLNESS? WE WANT TO HEAR IT!';
    $http.get(config.apiUrlWebService + "/dermvedaapi/get-wellness-articles").then(function successCallback(response) {
        if (response.status == 200) {
            $scope.featured = response.data.result.headerArticle;
            $scope.lifeStyle = response.data.result.lifeStyle;
            $scope.dietAndNutrition = response.data.result['Diet&Nutrition'];
            $scope.welnessFeaturedGuests = response.data.result.wellnessFeatured;
            $scope.faqs = response.data.result.herbalCorner;
        }
    });

    $scope.convertToEval = function(val) {
        if (typeof val !== 'undefined') {
            return eval(val)[1];
        }
    }

    angular.element(document).ready(function() {
        $timeout(function() {
            loadFunctions();
        }, 3000);
    });

}]);
