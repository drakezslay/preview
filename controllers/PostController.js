'use strict';

dermvedaApp.controller('PostController', ['$scope', '$http','$popover', function($scope, $http, $popover) {
  $scope.login = function(username,password){
    var loginDetalis = {};
    loginDetalis.username = username;
    loginDetalis.password = password;
    $http({
      method: 'POST',
      url: 'http://localhost:8080/dermveda-backend/src/public/user/login',
      data: loginDetalis,
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status, headers, config) {
      if ( data.authenticated === true) {
        window.location.href = '#/wellness';
      } else {
        $scope.errorMsg = "Login not correct";
      }
    })
    .error(function(data, status, headers, config) {
      $scope.errorMsg = 'Unable to submit form';
    })
  }
}]);

dermvedaApp.directive("customPopover", ["$popover", "$compile", function($popover, $compile) {
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var myPopover = $popover(element, {
                    title: 'My Title',
                    contentTemplate: 'partials/example.html',
                    html: true,
                    trigger: 'manual',
                    autoClose: true,
                    scope: scope
                });
                scope.showPopover = function() {

                }
            }
        }
    }]);
