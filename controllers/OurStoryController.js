'use strict';

dermvedaApp.controller('OurStoryController', function($scope, User, DocumentsService) {

    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("7b1e6ca3-ab7f-4914-96c9-f515b7f3fc7c").$promise.then(function(response) {
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == 'hippostd:html'){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });

    $scope.focusOnSubscribe = function(id) {
    	$("#"+id).focus();
    };
});
