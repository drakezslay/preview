'use strict';

dermvedaApp.controller('PrivacyController', function($scope, User, DocumentsService) {

    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("a0a7fc4b-a729-431e-95d7-75a1006b9db5").$promise.then(function(response) {
    	console.log(response);
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == "hippostd:html"){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });
});
