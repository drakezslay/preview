'use strict';

dermvedaApp.controller('HomeController', ['$scope', 'User', 'config', '$http', 'MetaService', '$rootScope', 'urlService', '$location', '$compile', function($scope, User, config, $http, MetaService, $rootScope, urlService, $location, $compile) {

    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Integrated Skin Health & Skin Care Knowledge | Dermveda", "Your trusted source for skin health information and personalized skin care tips. Create a free profile and learn about your skin symptoms today.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";

    var sliderloaded = false;
    $scope.loadCount = 0;
    $scope.apiUrl = config.apiUrlWebService;
    $scope.loggedIn = User.isLoggedIn();
    $rootScope.isLogged = $scope.loggedIn;

    User.update();

    $scope.urlList = {
        featured: "dermvedaapi/sorted-by-id?taxonomy=featured-guests-home-internal",
        erat: "dermvedaapi/sorted-by-id?taxonomy=emerging-research-and-trends-home",
        health: "dermvedaapi/sorted-by-id?taxonomy=health-home",
        beauty: "dermvedaapi/sorted-by-id?taxonomy=beauty-home",
        wellness: "dermvedaapi/sorted-by-id?taxonomy=wellness-home"
    };

    $scope.update = function() {
        $scope.offset = ($scope.currentPage - 1) * $scope.itemsPerPage;
        sliderloaded = false;
        $scope.loadArticlesToSlider('featured');
    }

    $scope.loadArticlesToSlider = function(clickedLink) {
        $(".tabcaruncelhome").html("<div class='loading'></div>");

        $http.get($scope.apiUrl + $scope.urlList[clickedLink]).then(function successCallback(response) {
            var newsSliderContents = new Array();

            if (response.data.status == 200) {
                $.each(response.data.result, function(i, val) {
                    var content = {
                        id: "",
                        name: "",
                        displayName: "",
                        bodyContent: "",
                        title: "",
                        image: "",
                        intro: ""
                    };
                    content.id = val.articleId;
                    content.name = val.name;
                    content.displayName = "no display name";
                    content.bodyContent = val.introduction;
                    content.title = val.title;
                    content.intro = val.introduction;
                    content.image = val.imageURL;

                    newsSliderContents.push(content);
                });
                $scope.buildSliderContent(newsSliderContents);
                $scope.loadCount++
            }
        });
    }

    $scope.urlService = urlService;

    $scope.buildSliderContent = function(arr) {

        if (sliderloaded) {
            $('.tabcaruncelhome').slick('unslick');
            sliderloaded = false;
        }

        $(".tabcaruncelhome").html("");

        $.each(arr, function(i, val) {
            var div = $("<div></div>");

            var content = $("<div></div>");
            content.addClass("pull-left tabcurcontent");

            var title = $("<h2></h2>");
            title.addClass("railwaylightfontfamily");
            title.html(val.title);

            var intro = $("<p></p>");
            intro.addClass("railwayreguler");
            intro.html(val.intro);

            var link = $("<a></a>");
            link.addClass("tabcaruncelhomelearnmore railwayboldfont");
            link.html("Learn More");
            link.attr("href", "#/"+ val.id );

            content.append(title);
            content.append(intro);
            content.append(link);

            div.append(content);

            $(".tabcaruncelhome").append(div);

            var image = $("<div></div>");
            image.addClass("pull-left tabcurimage hidden-xs");

            var imageLink = $("<a></a>");
            imageLink.addClass("imgHover");
            imageLink.attr("href", "#/" + val.id);
            imageLink.attr("data-toggle", "modal");
            imageLink.attr("data-target", "#accmodal01");

            var canvas = document.createElement("canvas");
            canvas.width = 456;
            canvas.setAttribute("href", "#/" + val.id);

            var ctx = canvas.getContext("2d");

            var img = new Image();
            img.onload = function() {
                canvas.height = (img.height < 300) ? canvas.width * (img.height / img.width) : 300;
                ctx.drawImage(img, 0, 0, 456, 325);
            }
            img.src = val.image;

            imageLink.append(canvas);
            image.append(imageLink);

            div.append(image);
        });

        function loadSliders() {
            $('.tabcaruncelhome').slick({
                slidesToShow: 1,
                arrows: true,
                centerMode: true,
                Default: 'ease',
                fade: true,
                speed: 800,
                focusOnSelect: true
            });
            sliderloaded = true;
        }

        loadSliders();
        $scope.loadCount++;
    }


    angular.element(document).ready(function() {
        loadFunctions();

        $('.bxslider').bxSlider({
            auto: true,
            pager: false,
            speed: 1000,
            mode: 'fade'
        });
    });

    $scope.update();

}]);
