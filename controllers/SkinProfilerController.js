'use strict';

var global_$scope = null;

dermvedaApp.controller('SkinProfilerController', function($http,$scope, $location, User, SkinProfiler, config, $rootScope, $timeout, MetaService, $window) {
    global_$scope = $scope;

    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Analyze Your Skin Type & Get a Skin Assessment | Dermveda", "Discover your unique skin type by answering a few simple questions. Try our skin analysis tool today and get expert skincare advice tailored to your needs.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";

    loadFunctions();

    User.update();

    if($location.absUrl().indexOf("results") != -1){
        var image =  window.localStorage.getItem('skinprofileImageData');
        var answerJSN = window.localStorage.getItem('skinprofileAnswers');

        $("#profileimg").attr("src",image);
        $("#profileimg").attr("style","border-radius:50%");
        $("#profileimg").attr("width", "46");
        $("#profileimg").attr("height", "46");

        $("#profileimghover").attr("src",image);
        $("#profileimghover").attr("style","border-radius:50%");
        $("#profileimghover").attr("width", "46");
        $("#profileimghover").attr("height", "46");

        $("#skinimgres").attr("src",image);
        $(".img-responsive").attr("src",image);

        $rootScope.skinImage = image;
        $("#skinimgres").attr("width", "127");
        $("#skinimgres").attr("height", "127");

        var resAnswe = JSON.parse(answerJSN);
        var answers = [];

        $.each(resAnswe, function(i, ans){
            answers.push(ans.questionAnswer[0].answerText);
        });

        $scope.hideWatermark = false;
        $scope.answerList = answers;

        populateResults(image,answerJSN, config);

        $scope.swithTab = function(tab)
        {
          $("#content div").removeClass("active");
          $("#"+tab).addClass("active");
        };

        $scope.toggleDropdown = function(id) {
            $(".filterbtngroup").removeClass("open");
            $("#"+id).attr("aria-expanded", false);
        }


        $(".medicinetypetabs ul li a").click(function(){
            var tab = $(this).attr("data-toggleTab");

            $("#content div").removeClass("active");
            $("#"+tab).addClass("active");
        });

        $scope.toggleDropdown = function(id) {
            console.log(id);
            $(".filterbtngroup").removeClass("open");
            $("#"+id).attr("aria-expanded", false);
        }

        $scope.saveToProfile = function(){

            if(!User.isLoggedIn()){
                $location.path( "/skin-type-profiler/register/skin-type-profiler;results" );
            } else {
                $rootScope.profileImage = image;
                var userID = localStorage.getItem('profileemail');
                $scope.resultObj.imageURL = image;
                $scope.resultObj.userID = userID;

                var data = JSON.stringify($scope.resultObj);

                $http({
                  method: 'POST',
                  data: data,
                  url: config.apiUrlWebService + 'dermveda1/save-analysed-profiler-data',
                  headers: {'Content-Type': 'application/json','Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='},
                })
                .then(function(response){
                      $location.path( "/profile" );
                });
            }
        }

    } else {
        window.localStorage.setItem('skinprofileAnswers', "");
        window.localStorage.setItem('skinprofileImageData', "");

        $scope.onNextTab = function(current, next){

            var count = 0;
            var questionAnswersArr = new Array();

            $.each($scope.QuestionsAndAnswers, function(i, question){
                var objectQuestion = { questionID: "", questionText: "", questionAnswer: []};
                var objectQuestionAnswer = {questionID:"", questionText: "", answerID: "", answerText: ""};

                var questionFieldID = "Question-"+question.questionID;
                var QAnswerSelect = $("#" + questionFieldID + " span.selectlabel").text();

                objectQuestion.questionID = question.questionID;
                objectQuestion.questionText = question.questionText;

                var answers = $("#QuestionUL-"+question.questionID).find("li a");
                $.each(answers, function(i, answer){
                    if($(answer).text() == QAnswerSelect){
                        count++;
    //                    return true;
                    }
                });

            });

            if (count == 3) {
                //$(".tablist .panel-group .panel .panel-collapse").slideUp();
    //        $(".tablist .panel-group .panel .panel-heading").removeClass("accodianselected");
                $(".tablist .panel-group .panel #" + current).removeClass("in");
                $(".tablist .panel-group .panel #" + current).parent().find(".panel-heading").addClass("accodianselected");;
                $(".tablist .panel-group .panel #" + next).parent().find(".panel-heading a").removeClass("collapsed");;
                $(".tablist .panel-group .panel #" + next).addClass("in");
                $(".tablist .panel-group .panel #" + next).slideDown('fast', function(){
                  var position = $("#head_"+next).position();
                  var headerHight = $(".sticky").height();
                  $window.scrollTo(position.left,230);
                });

                $(".alert").addClass("hidden");
                $(".alert").hide();
            } else {
                $(".alert").removeClass("hidden");
                $(".alert").show();
                return;
            }

        };

        $scope.analyseQA = function(){
            $(".preloderwrap").removeClass("hide");

            var questionAnswersArr = new Array();

            $.each($scope.QuestionsAndAnswers, function(i, question){
                var objectQuestion = { questionID: "", questionText: "", questionAnswer: []};
                var objectQuestionAnswer = {questionID:"", questionText: "", answerID: "", answerText: ""};

                var questionFieldID = "Question-"+question.questionID;
                var QAnswerSelect = $("#" + questionFieldID + " span.selectlabel").text();

                objectQuestion.questionID = question.questionID;
                objectQuestion.questionText = question.questionText;

                var answers = $("#QuestionUL-"+question.questionID).find("li a");
                $.each(answers, function(i, answer){
                    if($(answer).text() == QAnswerSelect){
                        objectQuestionAnswer.answerID = $(answer).data("answerid");
                        objectQuestionAnswer.answerText = QAnswerSelect;
    //                    return true;
                    }
                });

                if(QAnswerSelect.indexOf(question.questionText) < 0){
                    objectQuestion.questionAnswer.push(objectQuestionAnswer);
                    questionAnswersArr.push(objectQuestion);
                }

            });

            if(global_$scope.imageData == null || global_$scope.imageData == ""){
                $(".alert").html("Please select a image to proceed..");
                $(".alert").removeClass("hidden");
                $(".alert").show();
                return;
            }

            window.localStorage.setItem('skinprofileAnswers', JSON.stringify(questionAnswersArr));
            window.localStorage.setItem('skinprofileImageData', $scope.imageData);
            $location.path( "/skin-type-profiler/results" );
            Webcam.reset();
        };

        loadQuestionsAndAnswers(config);

        $scope.clickAnswerItem = function(id, answer){
            $("#Question-"+id + " span").text(answer);
        };
    }

    $scope.showCamera = true;
    $scope.isPhotoSet = false;

    $scope.takePhoto = function() {
        $('#takePhotoDiv').modal('show');
        Webcam.set({
            width: $(".cropit-preview").width(),
            height: $(".cropit-preview").height(),
            dest_width: 640,
            dest_height: 480,
            image_format: 'jpeg',
            jpeg_quality: 100,
            force_flash: false
        });
        Webcam.reset();
        Webcam.attach("#my_camera");
    }

    $scope.takeSnap = function() {
        var imageData;

        Webcam.snap(function(data_url) {
            imageData = data_url;
            // $("#my_camera_preview").attr('src', data_url);
            // Webcam.reset();
        });
        global_$scope.imageData = imageData;
        $('#imageuploader').attr("src",imageData);
        $("#imageuploader").width($(".skinimagepointermark").width());
        $("#imageuploader").height($(".skinimagepointermark").height());
        $scope.showCamera = true;
        $scope.hideWatermark = true;
        $(".imagedeletebtn").removeClass('hide');
        $("#imageuploadbtn").addClass('hide');
    }

    $scope.uploadPhotoText = "Upload Photo";

    $scope.deletePhoto = function() {
        $("#imageuploader").attr("src", "images/imageuploader.jpg");
        $("#imageuploader").attr("style", "{width: auto}");
        $(".imagedeletebtn").addClass('hide');
        $("#imageuploadbtn").removeClass('hide');
        $scope.hideWatermark = false;
        global_$scope.imageData = "";
    }

    $scope.openFileChooser = function () {
        $scope.hideWatermark = true;
        var isImageError = false;
        // $('.cropit-image-input').click(function() {
        //     $("#uploadError").html("");
        //     $("#uploadError").addClass('hide');
        //     $("#uploadError").removeClass('show');
        // });
        $('#imagecropdiv').modal('show');
        $('.image-editor').cropit({
            // smallImage: 'allow',
            onImageError: function() {
              isImageError = true;
            },
            onImageLoaded:function(){
              isImageError = false;
              $("#uploadError").addClass('hide');
              $("#uploadError").addClass('hidden');
              $("#uploadError").removeClass('show');
            }
        });

        $('.rotate-cw').click(function () {
            $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw').click(function () {
            $('.image-editor').cropit('rotateCCW');
        });

        $('#save_id').click(function () {
            var imageFile = $('#imageFile').val();
            if (!imageFile) {
                $("#uploadError").html("Please select image to proceed..");
                $("#uploadError").removeClass('hide');
                $("#uploadError").removeClass('hidden');
                $("#uploadError").addClass('show');
            }else if(isImageError == true){
              $("#uploadError").html("The uploaded photo is too small for analysis.<br/>Please upload a larger photo.");
              $("#uploadError").removeClass('hide');
              $("#uploadError").removeClass('hidden');
              $("#uploadError").addClass('show');
            } else if(imageFile && (isImageError == false)) {
                $(".imagedeletebtn").removeClass('hide');
                $("#imageuploadbtn").addClass('hide');
                var imageData = $('.image-editor').cropit('export');
                global_$scope.imageData = imageData;
                $("#imagecropdiv").modal('hide');
            }
        });

        $('#close_id').click(function () {
            $(".imagedeletebtn").addClass('hide');
            $("#imageuploadbtn").removeClass('hide');
            $scope.isPhotoSet = false;
            $scope.uploadPhotoText = "Upload Photo";
        });


        $('.export').click(function () {
            $(".imagedeletebtn").removeClass('hide');
            $("#imageuploadbtn").addClass('hide');
            var imageData = $('.image-editor').cropit('export');
            $('#imageuploader').attr("src",imageData);
        });
    };

});

function loadQuestionsAndAnswers(config){
    $.ajax({
        method: 'GET',
        type: 'GET',
        async: false,
        url: config.apiUrlWebService + 'dermveda1/get-face-skin-questions',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==");
        },
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='
        },
        success: function (response) {
            if(response.status = "200"){
                global_$scope.QuestionsAndAnswers = response.result;
            }
        }
    });
}

function previewFile() {
  var preview = $("#imageuploader");
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
      global_$scope.imageData = reader.result;
      preview.attr("src", reader.result);
      preview.attr("width", "268");
      preview.attr("height", "268");

  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

function populateResults(img, answersJSON, config){
  global_$scope.hideImageProcessing = false;
    var data = "{\"userID\": null, \"sessionID\": null, \"imageUrl\": \""+img+"\", \"listOfAnswers\": "+answersJSON+", \"dataPointValue\": null}";
    $.ajax({
        method: 'POST',
        type: 'POST',
        data: data,
        async: false,
        url: 'https://www.dermveda.com/dermveda/upload-image-for-profiler',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==");
        },
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='
        },
        success: function (response) {
            if(response.status = "200"){
                global_$scope.aryurvedic_med = response.result.aryurvedic_med;
                global_$scope.my_med = response.result.my_med;
                global_$scope.naturopathic_med = response.result.naturopathic_med;
                global_$scope.sessionID = response.result.sessionID;
                global_$scope.skin_rank = response.result.skin_rank;
                global_$scope.trad_chinese_med = response.result.trad_chinese_med;
                global_$scope.western_med = response.result.western_med;
                global_$scope.resultObj = response.result;
                global_$scope.hideImageProcessing = true;
            }
        }
    });

}
