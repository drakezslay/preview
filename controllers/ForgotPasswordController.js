'use strict';

dermvedaApp.controller('ForgotPasswordController', ['$scope', '$http', '$routeParams','$localStorage', '$window', '$location', 'User', 'config', function ($scope, $http, $routeParams,$localStorage, $window, $location, User, config) {

    $scope.userEmail = "";
    $scope.tokenValid = false;
    $scope.passwordError = "";

    $scope.checkForValidToken = function () {
        $http.get(config.apiUrlWebService + 'dermveda1/reset-password/' + $routeParams.token).success(function(response){
          if(response.result != null) {
              $scope.tokenValid = true;
              $scope.userEmail = response.result;
          } else {
              $location.path('/');
          }
        });
    }

    $scope.checkForValidToken();

    $scope.resetPassword = function () {

      if($scope.reEnterPassword.$invalid === false){
        $http.post(config.apiUrlWebService + 'dermveda1/update-password/' + $routeParams.token, '{"username": "' + $scope.userEmail + '", "password": "' + $scope.regData.confirmPass + '"}').success(function(response){
          if (response.status == "200") {
              if (window.localStorage) {
                  window.localStorage.setItem('user', response);
                  localStorage.setItem('profilename', response.result.firstName + " " + response.result.lastName);
                  localStorage.setItem('profilefirstname', response.result.firstName);
                  localStorage.setItem('profilelastname', response.result.lastName);
                  localStorage.setItem('profileemail', response.result.email);
                  localStorage.setItem('profileyob', response.result.yob);
                  localStorage.setItem('profileinterests', response.result.interests);
                  localStorage.setItem('profilegender', response.result.gender);
              }
              $location.path('/');
          }
        });
      }
    };

    }]);
