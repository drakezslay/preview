'use strict';

dermvedaApp.controller('AdvertiseController', function($scope, User, DocumentsService) {
    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("a9f0d7d3-37f1-4b76-b265-9f9b79e9ed0e").$promise.then(function(response) {
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == "hippostd:html"){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });
});
