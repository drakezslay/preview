dermvedaApp.controller('AdminController', function($scope, User, config, $http, $window) {

    if(localStorage.role != "ADMIN") {
        $window.location.href = "#/";
    }

	$scope.users = new Array();
	$scope.pages = new Array();
	$scope.usersPerPage = 25;
	$scope.currentPage = 1;
	$scope.from = 0;
	$scope.to = $scope.usersPerPage;
  $scope.searchEnabled = false;
  $scope.years = [];

    $scope.updateSearch = function() {
        if ($scope.searchQuery != "") {
            $scope.searchEnabled = true;
        } else {
            $scope.searchEnabled = false;
        }
    }

    $scope.getExportUsers = function() {
        var exportUsers = $scope.users;
        exportUsers.forEach(function(item, index) {
            delete item.id;
            delete item.username;
            delete item.password;
            delete item.enabled;
            delete item.encodeEnable;
            delete item.interests;
            delete item.insertedate;
            delete item["$$hashKey"];
        });
        return exportUsers;
    }

	var getUsers = function() {
		$http.get(config.apiUrlWebService + "dermvedaapi/all-users").then(function successCallback(response) {
            if(response.data.status == 200) {
            	$scope.users.length = 0;
                $scope.users = response.data.result;
                calculatePages();

            } else {
                  swal('Error!', 'No available records.', 'error');
            }
          });
	}

	getUsers();

	$scope.setUsersPerPage = function(number) {
		$scope.usersPerPage = number;
		$scope.from = 0;
		$scope.to = $scope.from + $scope.usersPerPage;
		calculatePages();
	};

	$scope.sortPredicate = 'firstName';
	$scope.sortString = "First Name"

	$scope.setSortPredicate = function(predicate, event) {
		$scope.sortPredicate = predicate;
		$scope.sortString = event.target.text;
		calculatePages();
	}

	$scope.changePage = function(page) {
		$scope.currentPage = page;
		$scope.from = (page - 1) * $scope.usersPerPage;
		$scope.to = $scope.from + $scope.usersPerPage;
	}

  for (var i=config.DOBStrat; i<config.DOBEnd + 1; i++){
    $scope.years.push(i);
  }

	$scope.signup = function () {

        if(error == true){
            if(isAgreed == false) {
                $('html,body').animate({scrollTop: $("body").offset().top}, 'slow');
            }
            return;
        }

        var payload = {};
        payload.password = $scope.regData.password;
        payload.enabled = 1;
        payload.encodeEnable = true;
        payload.firstName = $scope.regData.firstName;
        payload.lastName = $scope.regData.lastName;
        payload.email = $scope.regData.email;
        payload.gender = $scope.regData.gender;
        payload.yob = $scope.regData.yob;
        payload.interests = JSON.stringify($scope.regData.interests);
        payload = JSON.stringify(payload);

        $http({
          method:'POST',
          url:config.apiUrlWebService + 'dermvedaapi/signup',
          data:payload,
          headers: {'Content-Type': 'application/json', 'Authorization': 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA=='}
        }).then(function successCallback(response) {
          $scope.regData = {};
          swal('Success!', 'Successfully created a new user!', 'success');
          getUsers();
          }, function errorCallback(response) {
            swal('Error!', 'Could not create user.', 'error');
          });
    }

	var calculatePages = function() {
		$scope.pages.length = 0;
        var pagesToFit = 0;
        if( $scope.users.length % $scope.usersPerPage > 0) {
            pagesToFit = ($scope.users.length/$scope.usersPerPage) + 1;
        } else if ($scope.users.length < $scope.usersPerPage) {
            pagesToFit = 0;
        } else{
            pagesToFit = ($scope.users.length/$scope.usersPerPage);
        }
		for(var i=1; i<=pagesToFit; i++) {
        	$scope.pages.push(i);
        }
	}

	$scope.editUserView = function(user, index) {
        $scope.profile = {"firstName": "", "lastName": "", "email": "", "password": "", "gender": "", "yob": "", "interests": [], "index": 0}
        $scope.profile.firstName = user.firstName;
        $scope.profile.lastName = user.lastName;
        $scope.profile.email = user.email;
        $scope.profile.gender = user.gender;
        $scope.profile.yob = user.yob;
		$scope.profile.interests = JSON.parse(user.interests);
        $scope.profile.index = index;
	}

	$scope.dataUpdated = false;
	$scope.dataUpdateResponse = "";
	$scope.update = function (index) {
        var updateObj = {};
        updateObj.username = $scope.profile.email;
        updateObj.password = $scope.profile.password;
        updateObj.firstName = $scope.profile.firstName;
        updateObj.lastName = $scope.profile.lastName;
        updateObj.encodeEnable = true;
        updateObj.email = $scope.profile.email;
        updateObj.gender = $scope.profile.gender;
        updateObj.yob = $scope.profile.yob;
        updateObj.interests = JSON.stringify($scope.profile.interests);
        if ($scope.forfile.$valid) {
            $http.post(config.apiUrlWebService + 'dermvedaapi/update-user-profile-info', updateObj).then(function (response) {
                if(response.status == 200) {
                	$scope.dataUpdated = true;
                } else {
                	$scope.dataUpdateResponse = "Could not update user data";
                }
            });
        }

    };

    $scope.closeEdit = function() {
        if ($scope.dataUpdated == true) {
            $scope.dataUpdated = false;
            getUsers();
        }
    }

    $scope.viewUser = function(user) {
    	$scope.profile = user;
		$scope.profile.interests = user.interests;
    };

    $scope.userToDelete;
    $scope.setDeleteUser = function(user) {
    	$scope.userToDelete = user;
			$scope.deleteUser();
    }

    $scope.deleteUser = function() {

        $('#addfavmodal').modal('show');
       
    }


    $scope.deleteUserConfirm = function() {

            $http.get(config.apiUrlWebService + "dermvedaapi/delete-user?email=" + $scope.userToDelete.email).then(function successCallback(response) {
            if(response.data.status == 200) {
                swal('Deleted!','User '+ $scope.userToDelete.firstName+' '+$scope.userToDelete.lastName+' deleted successfully.', 'success');
                getUsers();
            }
          }, function errorCallback(response) {
            swal('Error!','Failed to delete user.', 'error');
          });
        
    }    

});
