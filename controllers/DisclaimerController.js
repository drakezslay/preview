'use strict';

dermvedaApp.controller('DisclaimerController', function($scope, User, DocumentsService) {
    loadFunctions();
    User.update();

    $scope.title = "";
    $scope.content = "";

    DocumentsService.getDocumentById("4904b9a4-83b6-417d-bea1-a4b5eb59c719").$promise.then(function(response) {
        $scope.title = response.displayName;

        angular.forEach(response.items, function(value) {
            if(value.type == "hippostd:html"){
                var contentToEdit = value.content;
                if(value.links != "undefined") {
                    angular.forEach(value.links, function(links) {
                        var urls = links.url.split("/");
                        var urlImageName = urls[urls.length - 1];
                        contentToEdit = contentToEdit.replace("data-hippo-link=\"" + urlImageName + "\"", 'src = "' + links.url + '"');
                    });
                }
                $scope.content = $scope.content + contentToEdit;
            }
        });
    });
});
