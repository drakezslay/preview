'use strict';
dermvedaApp.controller('BeautyController', ['$scope', '$http', 'config', 'documentService', '$location', '$q', '$filter', '$timeout', 'MetaService', '$rootScope', function($scope, $http, config, documentService, $location, $q, $filter, $timeout, MetaService, $rootScope) {

    $rootScope.metaservice = MetaService;
    $rootScope.metaservice.set("Beauty Tips for Naturally Healthy Skin, Body, Hair & Nails", "Discover a variety of practical beauty tips for naturally flawless skin, hair and nails written by dermatologists and skin care specialists.");
    $rootScope.metaAuthor = "Dermveda, Inc.";
    $rootScope.metaKeywords = "Skin, Science, Wellness";
    $scope.skinAndHairSection = {};

    $scope.bodyCareSection = {};
    $scope.featuredGuestSection = {};
    $scope.featured = {};
    $scope.faqs = [];
    $scope.questionFormTitle = 'Do you have a question about BEAUTY? WE WANT TO HEAR IT!'; //currently not using


    $http.get(config.apiUrlWebService + "dermveda1/get-beauty-articles").then(function successCallback(response) {
        if (response.status == 200) {
            $scope.featured = response.data.result.headerArticle;
            $scope.skinAndHairSection = response.data.result.NaturalSkincare;
            $scope.bodyCareSection = response.data.result.Skintervention;
            $scope.featuredGuestSection = response.data.result.BeautyFeatured;
            $scope.faqs = response.data.result.faq;
        }
    });

    $scope.convertToEval = function(val) {
        if (typeof val !== 'undefined') {
            var array = JSON.parse(val);
            return array[1];
        }
    }

    angular.element(document).ready(function() {
        $timeout(function() {
            loadFunctions();
        }, 3000);

    });
}]);
