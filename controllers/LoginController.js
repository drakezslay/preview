'use strict';

dermvedaApp.controller('LoginController', ['$scope', '$location', '$routeParams', 'User', 'loginServices', '$timeout', 'config', '$compile', '$rootScope', 'Facebook', '$http', '$window', '$controller','$injector', function ($scope, $location, $routeParams, User, loginServices, $timeout, config, $compile, $rootScope, Facebook, $http, $window, $controller,$injector) {

        $scope.forgetEmail = "";
        $scope.resetMsg = "";
        $scope.loginFormMsg = "";
        $scope.formResetInterval = 6000;

        $scope.forgetPassword = false;

        $scope.getCurrentlocation = function(){
            return $location.path();
        };

        $scope.loginWithFacebook = function() {
          Facebook.login(function(response) {
            Facebook.api('/me?fields=id,email,first_name,last_name', function(response2) {
                response2.access_token= response.authResponse.accessToken;
                $http.post(config.apiUrlWebService + "dermveda1/fb-auth", response2).then(function successCallback(response) {
                    if(response.status == 200) {
                        if (!response.data.result.isExsistingEmail) {
                            var obj = {
                                "firstName": response2.first_name,
                                "lastName": response2.last_name,
                                "email": response2.email
                            };
                            $window.location.href = "#signup/"+obj.firstName+"/"+obj.lastName+"/"+obj.email;

                        } else {
                            window.localStorage.setItem('user', response.data.result.user);
                            localStorage.setItem('profilename', response.data.result.user.firstName + " " + response.data.result.user.lastName);
                            localStorage.setItem('profilefirstname', response.data.result.user.firstName);
                            localStorage.setItem('profilelastname', response.data.result.user.lastName);
                            localStorage.setItem('profileemail', response.data.result.user.email);
                            localStorage.setItem('profileinterests', response.data.result.user.interests);
                            localStorage.setItem('profilegender', response.data.result.user.gender);
                            localStorage.setItem('role', response.data.result.user.role);

                            $location.path($location.path());
                            location.reload();
                        }
                    } else {
                        $("#loginError").html(response.data.result.response);
                    }
                  });

              });
          }, {
           scope: 'email',
           return_scopes: true
         });
        };

        $scope.resetPassword = function () {
            if ($scope.resetForm.$invalid == false) {
              $http.post(config.apiUrlWebService + 'dermveda1/send-reset-mail/?email=' + $scope.resetEmail + '&from=info@dermveda.com').success(function(response){
                if (response.status == "200") {
                    $scope.resetMsg = "A password reset link has been emailed to you.";
                    $scope.resetEmail = null;
                    $scope.resetForm.$setPristine();
                    $scope.resetForm.$setUntouched();
                } else {
                    $scope.resetMsg = "Email address not found. <u><a href='#/signup'>Click here</a></u> to sign up";
                    $timeout(function () {
                      $scope.resetForms();
                    }, $scope.formResetInterval);
                }
              });
            }
        }

        $scope.clickSignIn = function () {

            $scope.submitted = true;
            if ($scope.loginForm.$invalid == false) {
                var data = {
                    "username": $scope.username,
                    "password": $scope.password,
                };
                $http.post(config.apiUrlWebService + 'dermveda1/signin', JSON.stringify(data)).
                  success(function (data, status, headers) {
                      if (data.status == "200") {
                          if (window.localStorage) {
                              window.localStorage.setItem('user', data);
                              localStorage.setItem('profilename', data.result.firstName + " " + data.result.lastName);
                              localStorage.setItem('profilefirstname', data.result.firstName);
                              localStorage.setItem('profilelastname', data.result.lastName);
                              localStorage.setItem('profileemail', data.result.email);
                              localStorage.setItem('profileyob', data.result.yob);
                              localStorage.setItem('profileinterests', data.result.interests);
                              localStorage.setItem('profilegender', data.result.gender);
                              localStorage.setItem('role', data.result.role);
                              $http.post(config.apiUrlWebService + 'dermveda1/get-user-profile-info',  { 'username': data.result.email } ).then(function (response) {
                                  var skinIssues = JSON.parse(response.data.result.skin_issues);
                                  if (!localStorage.getItem('profilePic') && skinIssues && skinIssues.imageURL) {
                                      localStorage.setItem('profilePic', skinIssues.imageURL);
                                      $rootScope.profileImage = skinIssues.imageURL;
                                  }
                                  $('#signupContainer').css("display", "none");
                                  $('#profileContainer').css("display", "block");
                                  $rootScope.isLogged = true;
                              });
                              if(localStorage.role == "ADMIN") {
                                  $("#adminLink").show();
                              }
                              if($routeParams.redirect){
                                 $location.path($routeParams.redirect.replace(";", "/"))
                              }
                          }
                          $scope.username = null;
                          $scope.password = null;
                          $scope.loginForm.$setPristine();
                          $scope.loginForm.$setUntouched();
                          $scope.loginFormMsg = ''
                      } else {
                        $scope.loginFormMsg = data.result.message;
                        // $timeout(function () {
                        //   $scope.resetForms();
                        // }, $scope.formResetInterval);
                      }
                  }).
                   error(function (data, status, headers, config) {
                     if(typeof data !== 'undefined'){
                        $scope.loginFormMsg = data.result.message;
                        // $timeout(function () {
                        //   $scope.resetForms();
                        // }, $scope.formResetInterval);
                      }
                   });
            }

        };

        $scope.resetForms = function(){
          $scope.username = null;
          $scope.password = null;
          $scope.loginForm.$setPristine();
          $scope.loginForm.$setUntouched();
          $scope.loginFormMsg = ''
          $scope.resetEmail = null;
          $scope.resetForm.$setPristine();
          $scope.resetForm.$setUntouched();
        }

        angular.element(document).ready(function () {

            $timeout(function () {
              $scope.username = null;
              $scope.password = null;
              $scope.loginForm.$setPristine();
              $scope.loginForm.$setUntouched();
              $scope.loginFormMsg = ''
            }, 1000);

            $('#loginformclick').popover({
                placement: 'bottom',
                html:true,
                content: $compile($('#loginform').html())($scope)
             })

             $('#loginformclickmob').popover({
                placement: 'bottom',
                html:true,
                content: $compile($('#loginform').html())($scope)
             });

        });

    }]);
