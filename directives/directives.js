dermvedaApp.directive('newsletter', ['$http','$resource', 'config', function($http, $resource, config) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: function(scope, elem, attrs){
            if(elem.position === 'footer'){
                return 'directives/views/newsletterFooter.html';
            }
            if(elem.position === 'side'){
                return 'directives/views/newsletter.html';
            }
        },
        link: function(scope, iElement, iAttrs) {
            scope.subscribed = false;
            scope.subscribe = function(){
                if(scope.newsLetterForm.$valid){
                    $http.post(config.apiUrlWebService+'dermveda/mailchimp/subscribe', scope.newsletter).then(function(res){
                        if(res.status){
                            scope.subscribed = true;
                        } else {
                            scope.alreadtsubscribed = true;
                        }
                        scope.newsletter.email = null;
                        scope.subscribed = true;
                        scope.newsLetterForm.$setPristine();
                        scope.newsLetterForm.$setUntouched();
                    });
                }
            }
        }
    };
}]);

dermvedaApp.directive('contactForm', ['$http', '$resource','config', function($http, $resource, config){
    return {
        restrict: 'E',
        scope: {},
        templateUrl: function(scope, elem, attrs){
            return 'directives/views/contactForm.html';
        },
        link: function(scope, iElement, iAttrs) {
            scope.submit = function(){
                if(scope.contactForm.$valid){
                    scope.contact.type = 'Message';
                    scope.contact.subject = "Dermveda Contact Us";
                    $http.post(config.apiUrlWebService+'dermveda/awsses/sendmail', JSON.stringify(scope.contact)).then(function(res){
                        if(res.status){
                            scope.submitted = true;
                            scope.contact = {};
                            scope.contactForm.$setPristine();
                            scope.contactForm.$setUntouched();
                        } else {
                            scope.submitted = false;
                        }
                    });
                }
            }
        }
    };
}]);

dermvedaApp.directive('faqForm', ['$http','$resource','config', function($http, $resource, config){
    return {
        restrict: 'E',
        scope: {},
        templateUrl: function(scope, elem, attrs){
            return 'directives/views/faqForm.html';
        },
        link: function(scope, iElement, iAttrs) {
            scope.submitted = false;
            scope.submit = function(){
                if(scope.faqForm.$valid){
                    scope.submitted = true;
                    scope.faq.subject = "Beauty - Dear Dermveda Submission";
                    scope.faq.type = 'Question';
                    scope.faq.body += "<br>" + window.location.href;
                    $http.post(config.apiUrlWebService+'dermveda/awsses/sendmail', JSON.stringify(scope.faq)).then(function(res){
                        if(res.status){
                            scope.faq = {};
                            scope.faqForm.$setPristine();
                            scope.faqForm.$setUntouched();
                        } else {
                            scope.submitted = false;
                        }
                    });
                }
            }
        }
    };
}]);

dermvedaApp.directive('ngConfirmField', function () {
    return {
        require: 'ngModel',
        link: function (scope, iElement, iAttrs, ngModelCtrl) {

          var updateValidity = function () {
            var viewValue = ngModelCtrl.$viewValue;
            var isValid = isFieldValid();
            ngModelCtrl.$setValidity('noMatch', isValid);
            return isValid ? viewValue : undefined;
          };

          var isFieldValid = function () {
            return ngModelCtrl.$viewValue === currentConfirmAgainstValue();
          };

          function currentConfirmAgainstValue() {
            return scope.$eval(iAttrs.confirmAgainst);
          }

          ngModelCtrl.$parsers.push(updateValidity);

          scope.$watch('confirmAgainst', updateValidity);
          scope.$watch(function () {
            return currentConfirmAgainstValue();
          }, updateValidity);
        }
    };
});

	dermvedaApp.directive('exportToCsv',function(){
  	return {
    	restrict: 'A',
    	link: function (scope, element, attrs) {
    		var el = element[0];
	        element.bind('click', function(e){
	        	var table = e.target.nextElementSibling;
	        	var csvString = '';
	        	for(var i=0; i<table.rows.length;i++){
	        		var rowData = table.rows[i].cells;
	        		for(var j=0; j<rowData.length;j++){
	        			csvString = csvString + rowData[j].innerHTML + ",";
	        		}
	        		csvString = csvString.substring(0,csvString.length - 1);
	        		csvString = csvString + "\n";
			    }
	         	csvString = csvString.substring(0, csvString.length - 1);
	         	var a = $('<a/>', {
		            style:'display:none',
		            href:'data:application/octet-stream;base64,'+btoa(csvString),
		            download:'User report.csv'
		        }).appendTo('body')
		        a[0].click()
		        a.remove();
	        });
    	}
  	}
	});
