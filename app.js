'use strict';

var dermvedaApp = angular.module('dermvedaApp', ['validation', 'validation.rule', 'ngRoute', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'mgcrea.ngStrap', 'ngCookies', 'ngStorage', 'slick', 'facebook', 'ngCsv']);


dermvedaApp.service('MetaService', function() {
    var metaTitle = 'Dermveda';
    var metaDescription = '';
    return {
        set: function(newTitle, newMetaDescription) {
            metaDescription = newMetaDescription;
            metaTitle = newTitle;
        },
        metaTitle: function() {
            return metaTitle;
        },
        metaDescription: function() {
            return metaDescription;
        }
    }
});

dermvedaApp.directive('isActiveNav', ['$location', function($location) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            scope.location = $location;
            scope.$watch('location.path()', function(currentPath) {
                if ('/#' + currentPath === element[0].attributes['href'].nodeValue) {
                    element.parent().addClass('active');
                } else {
                    element.parent().removeClass('active');
                }
            });
        }
    };
}]);
dermvedaApp.constant('config', {
    appName: 'Dermveda',
    appVersion: 2.0,
    apiUrl: 'https://www.dermveda.com/rest/api/documents/',
   // apiUrl: 'http://ec2-52-10-189-92.us-west-2.compute.amazonaws.com:8080/rest/api/documents/',
    
    apiUrlWebService: 'https://www.dermveda.com/',
    //apiUrlWebService: 'http://ec2-52-10-189-92.us-west-2.compute.amazonaws.com:8080/',
    DOBStrat: 1900,
    DOBEnd: 2002
});

dermvedaApp.config([
    '$validationProvider',
    function(
        $validationProvider
    ) {
        $validationProvider
            .setExpression({
                confirmEmail: function(value, scope, element, attrs) {
                    return value === attrs.matchEmail;
                }
            })
            .setDefaultMsg({
                confirmEmail: {
                    error: 'Email does not match',
                    success: 'Confirmed!'
                }
            });

        $validationProvider.showSuccessMessage = false;

        angular.extend($validationProvider, {
            validCallback: function(element) {
                $(element).removeClass('error');
            },
            invalidCallback: function(element) {
                $(element).addClass('error');
            }
        });
    }
]);

dermvedaApp.config(['$compileProvider', function($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

dermvedaApp.config(function(FacebookProvider) {
    // Set your appId through the setAppId method or
    // use the shortcut in the initialize method directly.
    FacebookProvider.init('613998448748734');
});

dermvedaApp.config(function($routeProvider, $popoverProvider, $compileProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController',
        title: 'Home'
    }).when('/wellness', {
        templateUrl: 'views/wellness.html',
        controller: 'WellnessController',
        title: 'Wellness'
    }).when('/beauty', {
        templateUrl: 'views/beauty.html',
        controller: 'BeautyController',
        title: 'Beauty'
    }).when('/health', {
        templateUrl: 'views/health.html',
        controller: 'HealthController',
        title: 'Health'
    }).when('/signout', {
        templateUrl: 'views/signout.html',
        controller: 'LogoutController',
        title: 'Sign-out'
    }).when('/signup/:redirect?', {
        templateUrl: 'views/signup.html',
        controller: 'SignupController',
        title: 'Sign-up'
    }).when('/signup/:fname/:lname/:email?', {
        templateUrl: 'views/signup.html',
        controller: 'SignupController',
        title: 'Sign-up'
    }).when('/admin', {
        templateUrl: 'views/usermanagement.html',
        controller: 'AdminController',
        title: 'Sign-up'
    }).when('/skin-type-profiler/register/:redirect?', {
        templateUrl: 'views/signin_signup.html',
        controller: 'LoginController',
        title: 'Skin type sign-in'
    }).when('/educator-symptoms/register/:redirect?', {
        templateUrl: 'views/signin_signup.html',
        controller: 'LoginController',
        title: 'Educator sign-in'
    }).when('/profile/:tag?', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileController',
        title: 'Profile'
    }).when('/guestView/:id?', {
        templateUrl: 'views/featured_guest_view.html',
        controller: 'FeaturedGuestViewController',
        title: 'Featured Guest'
    }).when('/skin-type-profiler', {
        templateUrl: 'views/skintool/skin_type_profiler.html',
        controller: 'SkinProfilerController'
    }).when('/skin-type-profiler/results', {
        templateUrl: 'views/skintool/profiler_results.html',
        controller: 'SkinProfilerController'
    }).when('/educator-symptoms', {
        templateUrl: 'views/educatortool/educator_symptoms.html',
        controller: 'EducatorController'
    }).when('/educator-symptoms-disclaimer', {
        templateUrl: 'views/educatortool/educator_symptoms_disclaimer.html',
        controller: 'EducatorController'
    }).when('/educator-symptoms/results', {
        templateUrl: 'views/educatortool/educator_symptoms_result.html',
        controller: 'EducatorController'
    }).when('/terms', {
        templateUrl: 'views/terms.html',
        controller: 'TermsController'
    }).when('/privacy', {
        templateUrl: 'views/privacy.html',
        controller: 'PrivacyController'
    }).when('/featured-guests', {
        templateUrl: 'views/featured_guests.html',
        controller: 'FeaturedGuestsController'
    }).when('/disclaimer', {
        templateUrl: 'views/disclaimer.html',
        controller: 'DisclaimerController'
    }).when('/careers', {
        templateUrl: 'views/careers.html',
        controller: 'CareersController'
    }).when('/contributors', {
        templateUrl: 'views/contributors.html',
        controller: 'ContributorsController'
    }).when('/advertise', {
        templateUrl: 'views/advertise.html',
        controller: 'AdvertiseController'
    }).when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactController'
    }).when('/ourstory', {
        templateUrl: 'views/our.html',
        controller: 'OurStoryController'
    }).when('/404', {
        templateUrl: 'views/page404.html'
    }).when('/:uuid', {
        templateUrl: 'views/detail.html',
        controller: 'DocumentsController'
    }).when('/search/:terms/page/:page/filter/:filter', {
        templateUrl: 'views/search.html',
        controller: 'SearchController',
        title: 'Search Results'
    }).when('/reset-password/:token?', {
        templateUrl: 'views/forgot_password.html',
        controller: 'ForgotPasswordController'
    }).when('/categoryResults/:category?', {
        templateUrl: 'views/category_results.html',
        controller: 'CategoryResultsController'
    }).when('/tag/:terms/page/:page/filter/:filter', {
        templateUrl: 'views/tag_search.html',
        controller: 'TagResultsController',
        title: 'Tag Results'
    }).when('/guestView/:uuid', {
        templateUrl: 'views/detail.html',
        controller: 'DocumentsController'
    }).otherwise({
        redirectTo: '/#'
    });
    angular.extend($popoverProvider.defaults, {
        html: true
    });
    $compileProvider.debugInfoEnabled(false);
});

dermvedaApp.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {

        if (current.hasOwnProperty('$$route')) {

            $rootScope.title = current.$$route.title;
        }
    });
}]);

dermvedaApp.run(function($http) {
    $http.defaults.headers.common = {};
    $http.defaults.headers.post = {};
    $http.defaults.headers.put = {};
    $http.defaults.headers.patch = {};
    $http.defaults.headers.common['Content-Type'] = 'application/json';
    $http.defaults.headers.common['Authorization'] = 'Basic ZGVybXZlZGFndWVzdDpkZXJtdmVkYUBndWVzdA==';
});
